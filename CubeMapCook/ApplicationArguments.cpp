// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "ApplicationArguments.h"
#include "Cube.h"
#include "Utility/Formats.h"
#include "Utility/tinyexpr/tinyexpr.h"

using namespace Math;
using namespace Utility;

//////////////////////////////////////////////////////////////////////////////

const NamedValue ApplicationArguments::sNamedOptions[] =
{
	{ L"ambientbrdf",			OPT_AMBIENT_BRDF },
	{ L"alpha",					OPT_ALPHA },
	{ L"cosine",				OPT_COSINE },
	{ L"dx10",					OPT_DX10 },
	{ L"format",				OPT_FORMAT },
	{ L"gammain",				OPT_GAMMA_IN },
	{ L"gammaout",				OPT_GAMMA_OUT },
	{ L"linearscale",			OPT_LINEAR_SCALE },
	{ L"orientin",				OPT_ORIENT_IN },
	{ L"orientout",				OPT_ORIENT_OUT },
	{ L"out",					OPT_OUTPUT_FILE },
	{ L"normalizeirradiance",	OPT_NORMALIZE_IRRADIANCE },
	{ L"size",					OPT_SIZE },
	{ L"tolonglat",				OPT_TO_LONG_LAT },
	{ nullptr,					0 }
};

//////////////////////////////////////////////////////////////////////////////

static_assert(ApplicationArguments::OPT_MAX <= 32, "Options is a uint32 bitfield");

//////////////////////////////////////////////////////////////////////////////

static bool sDecomposeOrientationsArgument(wchar_t* inString, Mat2x2 (&outOrientations)[6])
{
	wchar_t* ptr = inString;
	int face = 0;
	for (; face  < 6;) 
	{
		wchar_t* ptr_end = ptr;

		while (*ptr_end != 0 && *ptr_end != ',')
			++ptr_end;

		if (!gGetOrientationFromString(ptr, ptr_end, outOrientations[face]))
			return false;

		ptr = ptr_end;

		++face;

		if (*ptr == 0)
			break;

		ASSERT(*ptr == ',');
		++ptr;
	}

	return face == 6 && *ptr == 0;
}

//////////////////////////////////////////////////////////////////////////////

static bool sGetPositiveFloatValue(wchar_t* inValue, float& outValue)
{
	wchar_t* end = 0;
	outValue = (float)wcstod(inValue, &end);

	if (outValue <= 0.0f || *end != 0)
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////////////

static bool sGetPositiveIntValue(wchar_t* inValue, int& outValue)
{
	wchar_t* end = 0;
	long result = wcstol(inValue, &end, 10);

	if (result <= 0 || *end != 0)
		return false;

	outValue = (int)result;
	return true;
}

//////////////////////////////////////////////////////////////////////////////

void gPrintApplicationArgumentsHelp()
{
	wprintf(L"Usage: CubeCook <options> <input files>\n");
	wprintf(L"\n");
	wprintf(L"   -ambientbrdf                    Alternative tool mode, generating a 2D\n");
	wprintf(L"                                   lookup texture representing the amount of\n");
	wprintf(L"                                   radiance in an particular direction for a\n");
	wprintf(L"                                   particular roughness when lit equally from\n");
	wprintf(L"                                   all directions. Output is a red-green color\n");
	wprintf(L"                                   pair for scale-and-biasing the fresnel R0\n");
	wprintf(L"                                   term. Requires -alpha, -cosine, and does\n");
	wprintf(L"                                   not support -format, -gammain, -orientin\n");
	wprintf(L"                                   and -orientout.\n\n");
	wprintf(L"   -alpha <expression>             When used to filter cubemaps, alpha as a \n");
	wprintf(L"                                   GGX roughness function of 'mipresolution'\n");
	wprintf(L"                                   and/or 'miplevel'. The defaut is equal to\n");
	wprintf(L"                                   '1/mipresolution'. When used with \n");
	wprintf(L"                                   -ambientbrdf, alpha must be a function of\n");
	wprintf(L"                                   'u' and/or 'v' instead. Supports most C-style\n");
	wprintf(L"                                   math functions.\n\n");
	wprintf(L"   -cosine <expression>            When used with -ambientbrdf, cosine must be\n");
	wprintf(L"                                   a function of 'u' and/or 'v', representing\n");
	wprintf(L"                                   the cosine of the angle between the normal\n");
	wprintf(L"                                   and the view direction. Supports most C-style\n");
	wprintf(L"                                   math functions.\n\n");
	wprintf(L"   -dx10                           Force use of DX10 extended header.\n\n");
	wprintf(L"   -format <format>                Channel/compression format to output.\n");
	wprintf(L"                                   Default is same as input format.\n\n");
	wprintf(L"   -gammain <string/float>         Gamma function to convert input from.\n");
	wprintf(L"                                   E.g. 'sRGB' or '2.2'. Default is '1.0'.\n\n");
	wprintf(L"   -gammaout <string/float>        Gamma function to convert output to.\n");
	wprintf(L"                                   E.g. 'sRGB' or '2.2'. Default is '1.0'.\n\n");
	wprintf(L"   -linearscale <float>            Multiplier over all channgels applied after\n");
	wprintf(L"                                   -gammain and before -gammaout.\n\n");
	wprintf(L"   -orientin <string>,<string>,... Orientation of all 6 faces before filtering,\n");
	wprintf(L"                                   specified as a sequence of symbols per face:\n");
	wprintf(L"                                   L (rotate left/CCW), R (rotate right/CW)\n");
	wprintf(L"                                   H (flip horizontally), V (flip vertically).\n\n");
	wprintf(L"   -orientout <string>,<string>,.. Reorientation of all 6 faces after filtering,\n");
	wprintf(L"                                   specified in the same format as -orientin.\n\n");
	wprintf(L"   -out <filename>                 Output filename for the generated content.\n\n");
	wprintf(L"   -normalizeirradiance            Normalize the result such that when multiplied\n");
	wprintf(L"                                   by the SH1 of the original irradiance, the\n");
	wprintf(L"                                   unnormalized data times 0.18 is reconstructed.");
	wprintf(L"   -size <integer>                 Max dimensions of output. Currently only\n");
	wprintf(L"                                   supported in combination with -ambientbrdf.\n\n\n");
	wprintf(L"   <format>: ");
	gPrintNamedValues(13, gNamedSupportedFormats);
	wprintf(L"\nhttps://bitbucket.org/gdecarpentier/cubemapcook.\n");
	wprintf(L"Giliam de Carpentier, 2016. http://www.decarpentier.nl.\n");
}

//////////////////////////////////////////////////////////////////////////////

bool gGetApplicationArguments(int inArgC, wchar_t* inArgV[], ApplicationArguments& outProcessArguments)
{
	outProcessArguments.mAlphaExpression = nullptr;
	outProcessArguments.mCosineExpression = nullptr;
	outProcessArguments.mInputColorConvertParameters = gGetColorConvertParametersPassthrough();
	outProcessArguments.mInputFilenames.clear();
	for (int i = 0; i < 6; ++i)
		outProcessArguments.mInputOrientations[i] = Mat2x2::Identity();
	outProcessArguments.mLinearScale = 1.0f;
	outProcessArguments.mOptions = 0;
	outProcessArguments.mOutputColorConvertParameters = gGetColorConvertParametersPassthrough();
	outProcessArguments.mOutputFilename[0] = 0;
	outProcessArguments.mOutputFormat = DXGI_FORMAT_UNKNOWN;
	for (int i = 0; i < 6; ++i)
		outProcessArguments.mOutputOrientations[i] = Mat2x2::Identity();
	outProcessArguments.mSize = 0;

	float dummy;

	for (int arg_index = 1; arg_index < inArgC; arg_index++)
	{
		wchar_t* arg_string = inArgV[arg_index];


		if (('-' == arg_string[0]) || ('/' == arg_string[0]))
		{
			arg_string++;
			wchar_t* value;

			for (value = arg_string; *value && ('=' != *value); value++);

			if (*value)
				*value++ = 0;

			uint32_t option = gLookupValueByName(arg_string, ApplicationArguments::sNamedOptions);

			if (!option)
			{
				wprintf(L"Syntax error: Invalid/unknown argument specified: %ls.\n", arg_string);
				return false;
			}

			if (outProcessArguments.mOptions & (1 << option))
			{
				wprintf(L"Syntax error: Found argument more than once: %ls.\n", arg_string);
				return false;
			}
			outProcessArguments.mOptions |= 1 << option;

			if (ApplicationArguments::OPT_AMBIENT_BRDF != option &&
				ApplicationArguments::OPT_DX10 != option &&
				ApplicationArguments::OPT_NORMALIZE_IRRADIANCE != option &&
				ApplicationArguments::OPT_TO_LONG_LAT != option)
			{
				if (!*value)
				{
					if ((arg_index + 1 >= inArgC))
					{
						wprintf(L"Syntax error: Missing parameter for last argument.\n");
						return false;
					}

					arg_index++;
					value = inArgV[arg_index];
				}
			}

			bool success = true;

			switch (option)
			{
			case ApplicationArguments::OPT_ALPHA:
				outProcessArguments.mAlphaExpression = value;
				break;
		
			case ApplicationArguments::OPT_COSINE:
				outProcessArguments.mCosineExpression = value;
				break;
		
			case ApplicationArguments::OPT_FORMAT:
				outProcessArguments.mOutputFormat = (DXGI_FORMAT)gLookupValueByName(value, gNamedSupportedFormats);
				success = !!outProcessArguments.mOutputFormat;
				break;

			case ApplicationArguments::OPT_GAMMA_IN:
				success = gGetColorConvertParametersFromStringToLinear(value, outProcessArguments.mInputColorConvertParameters);
				break;

			case ApplicationArguments::OPT_GAMMA_OUT:
				success = gGetColorConvertParametersFromLinearToString(value, outProcessArguments.mOutputColorConvertParameters);
				break;

			case ApplicationArguments::OPT_LINEAR_SCALE:
				success = sGetPositiveFloatValue(value, outProcessArguments.mLinearScale);
				break;

			case ApplicationArguments::OPT_ORIENT_IN:
				success = sDecomposeOrientationsArgument(value, outProcessArguments.mInputOrientations);
				break;

			case ApplicationArguments::OPT_ORIENT_OUT:
				success = sDecomposeOrientationsArgument(value, outProcessArguments.mOutputOrientations);
				break;

			case ApplicationArguments::OPT_OUTPUT_FILE:
				wcscpy_s(outProcessArguments.mOutputFilename, MAX_PATH, value);
				break;

			case ApplicationArguments::OPT_SIZE:
				success = sGetPositiveIntValue(value, outProcessArguments.mSize);
				break;

			default:
				break;
			}

			if (!success)
			{
				wprintf(L"Syntax error: Invalid value specified for -%ls: %ls.\n", arg_string, value);
				return false;
			}
		}
		else
		{
			outProcessArguments.mInputFilenames.resize(outProcessArguments.mInputFilenames.size() + 1);
			wcscpy_s(outProcessArguments.mInputFilenames.back(), MAX_PATH, arg_string);
		}
	}

	if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_AMBIENT_BRDF)) != 0)
	{
		if (outProcessArguments.mAlphaExpression == nullptr)
		{
			wprintf(L"Syntax error: Missing the required -alpha option that is required for -ambientbrdf.\n");
			return false;
		}

		if (!gEvaluateUVExpression(outProcessArguments.mAlphaExpression, 1.0f, 1.0f, dummy))
		{
			wprintf(L"Syntax error: Invalid expression specified for -alpha: %ls. Should be a function of 'u' and/or 'v'.\n", outProcessArguments.mAlphaExpression);
			return false;
		}

		if (outProcessArguments.mCosineExpression == nullptr)
		{
			wprintf(L"Syntax error: Missing the required -cosine option that is required for -ambientbrdf.\n");
			return false;
		}

		if (!gEvaluateUVExpression(outProcessArguments.mCosineExpression, 1.0f, 1.0f, dummy))
		{
			wprintf(L"Syntax error: Invalid expression specified for -cosine: %ls. Should be a function of 'u' and/or 'v'.\n", outProcessArguments.mCosineExpression);
			return false;
		}

		if (outProcessArguments.mSize <= 0)
		{
			wprintf(L"Syntax error: Requires a positive number for parameter -size when using -ambientbrdf.\n");
			return false;
		}

		if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_FORMAT)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_GAMMA_IN)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_ORIENT_IN)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_ORIENT_OUT)) != 0)
		{
			wprintf(L"Syntax error: Currently, the -ambientbrdf cannot be used together with the -format, -gammain, -orientin and -orientout options.\n");
			return false;
		}
	}
	else if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_TO_LONG_LAT)) != 0)
	{
		if (outProcessArguments.mSize <= 0)
		{
			wprintf(L"Syntax error: Missing the required (positive) -size option that is required for -tolonglat.\n");
			return false;
		}

		if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_ALPHA)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_COSINE)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_FORMAT)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_GAMMA_OUT)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_ORIENT_IN)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_ORIENT_OUT)) != 0)
		{
			wprintf(L"Syntax error: Currently, the -tolonglat cannot be used together with the -alpha, -cosine, -format and -gammaout options.\n");
			return false;
		}
	}
	else
	{
		if (outProcessArguments.mAlphaExpression != nullptr)
		{
			if (!gEvaluateResolutionLevelExpression(outProcessArguments.mAlphaExpression, 256, 0, dummy))
			{
				wprintf(L"Syntax error: Invalid expression specified for -alpha: %ls. Should be a function of 'mipresolution' and/or 'miplevel'.\n", outProcessArguments.mAlphaExpression);
				return false;
			}
		}
		else
		{
			outProcessArguments.mAlphaExpression = L"1/mipresolution";
		}

		if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_COSINE)) != 0 ||
			(outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_SIZE)) != 0)
		{
			wprintf(L"Syntax error: -cosine and -size may only be used together with the -ambientbrdf option.\n");
			return false;
		}

		if (outProcessArguments.mInputFilenames.empty())
		{
			wprintf(L"Syntax error: Missing required input filenames\n");
			return false;
		}
	}

	if ((outProcessArguments.mOptions & (1 << ApplicationArguments::OPT_OUTPUT_FILE)) == 0)
	{
		wprintf(L"Syntax error: Missing required -out parameter\n");
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

bool gEvaluateResolutionLevelExpression(const wchar_t* inExpression, int inResolution, int inLevel, float& outResult)
{
	std::wstring wstr(inExpression);
	std::string str(wstr.begin(), wstr.end());

	struct Closures
	{
		double mResolution;
		double mLevel;

		static double GetResolution(void* inContext) { return ((Closures*)inContext)->mResolution; }
		static double GetMipLevel(void* inContext) { return ((Closures*)inContext)->mLevel; }
	};

	Closures closures = { (double)inResolution, (double)inLevel };

    te_variable vars[] = {{"mipresolution",	&Closures::GetResolution, TE_CLOSURE0, &closures}, 
						  {"miplevel",		&Closures::GetMipLevel, TE_CLOSURE0, &closures}};

    int err;
    te_expr *expr = te_compile(str.c_str(), vars, sizeof(vars) / sizeof(te_variable), &err);
	outResult = (float)te_eval(expr);
	te_free(expr);

	return err == 0 && !std::isnan(outResult);
}

//////////////////////////////////////////////////////////////////////////////

bool gEvaluateUVExpression(const wchar_t* inExpression, float inU, float inV, float& outResult)
{
	std::wstring wstr(inExpression);
	std::string str(wstr.begin(), wstr.end());

	struct Closures
	{
		double mU;
		double mV;

		static double GetU(void* inContext) { return ((Closures*)inContext)->mU; }
		static double GetV(void* inContext) { return ((Closures*)inContext)->mV; }
	};

	Closures closures = { (double)inU, (double)inV };

    te_variable vars[] = {{"u",	&Closures::GetU, TE_CLOSURE0, &closures}, 
						  {"v",	&Closures::GetV, TE_CLOSURE0, &closures}};

    int err;
    te_expr *expr = te_compile(str.c_str(), vars, sizeof(vars) / sizeof(te_variable), &err);
	outResult = (float)te_eval(expr);
	te_free(expr);

	return err == 0 && !std::isnan(outResult);
}
//////////////////////////////////////////////////////////////////////////////

