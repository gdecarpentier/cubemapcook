// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "ApplicationArguments.h"
#include "Utility/ColorConvert.h"
#include "Utility/NamedValues.h"
#include "Utility/Path.h"
#include "Math/Mat.h"
#include <dxgiformat.h>
#include <list>
#include <vector>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>

//////////////////////////////////////////////////////////////////////////////

struct ApplicationArguments
{
	wchar_t*							mAlphaExpression;
	wchar_t*							mCosineExpression;
	Utility::ColorConvertParameters		mInputColorConvertParameters;
	std::list<Utility::Path>			mInputFilenames;
	Math::Mat2x2						mInputOrientations[6];
	float								mLinearScale;
	uint32_t							mOptions;
	Utility::ColorConvertParameters		mOutputColorConvertParameters;
	wchar_t								mOutputFilename[MAX_PATH];
	DXGI_FORMAT							mOutputFormat;
	Math::Mat2x2						mOutputOrientations[6];
	int									mSize;

	enum OPTIONS    // Note: Options below assumes 32 or less options.
	{
		OPT_AMBIENT_BRDF = 1,
		OPT_ALPHA,
		OPT_COSINE,
		OPT_DX10,
		OPT_FORMAT,
		OPT_GAMMA_IN,
		OPT_GAMMA_OUT,
		OPT_LINEAR_SCALE,
		OPT_ORIENT_IN,
		OPT_ORIENT_OUT,
		OPT_OUTPUT_FILE,
		OPT_NORMALIZE_IRRADIANCE,
		OPT_SIZE,
		OPT_TO_LONG_LAT,
		OPT_MAX
	};

	static const Utility::NamedValue sNamedOptions[];
};

void gPrintApplicationArgumentsHelp();

bool gGetApplicationArguments(int inArgC, wchar_t* inArgV[], ApplicationArguments& outProcessArguments);

bool gEvaluateResolutionLevelExpression(const wchar_t* inExpression, int inResolution, int inLevel, float& outResult);
bool gEvaluateUVExpression(const wchar_t* inExpression, float inU, float inV, float& outResult);

//////////////////////////////////////////////////////////////////////////////
