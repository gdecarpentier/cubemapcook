// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Cube.h"
#include "Math/Algorithm.h"
#include "Math/SH1.h"
#include "Math/Mat.h"
#include "directxtex.h"

using namespace DirectX;
using namespace Math;

//////////////////////////////////////////////////////////////////////////////

namespace {

static Vec4 sUVToXYZScalePerFace[] = { 
	Vec4( 0.0f, -2.0f, -2.0f,  0.0f), 
	Vec4( 0.0f, -2.0f,  2.0f,  0.0f),  
	Vec4( 2.0f,  0.0f,  0.0f,  2.0f), 
	Vec4( 2.0f,  0.0f,  0.0f, -2.0f), 
	Vec4( 2.0f, -2.0f,  0.0f,  0.0f), 
	Vec4(-2.0f, -2.0f,  0.0f,  0.0f) };

static Vec4 sUVToXYZBiasPerFace[] = { 
	Vec4( 1.0f,  1.0f,  1.0f, 0.0f), 
	Vec4(-1.0f,  1.0f, -1.0f, 1.0f),
	Vec4(-1.0f,  1.0f, -1.0f, 2.0f), 
	Vec4(-1.0f, -1.0f,  1.0f, 3.0f),
	Vec4(-1.0f,  1.0f,  1.0f, 4.0f), 
	Vec4( 1.0f,  1.0f, -1.0f, 5.0f) };

} // namespace 

//////////////////////////////////////////////////////////////////////////////

static float sGetSolidAngle(float x0, float y0, float d)
{
	// Applying the algorithm from Van Oosterom, A; Strackee, J (1983). "The Solid Angle of a Plane Triangle". IEEE Trans. Biom. Eng. BME-30 (2): 125�126. doi:10.1109/TBME.1983.325207.
	// One time for the triangle t0 consisting of vertices (x0,y0,1), (x0+d,y0,1) and (x0,y0+d,1), and one time for triangle t1 consisting of vertices (x0+d,y0,1), (x0+d,y0+d,1) and (x0,y0+d,1).
	// Note that for both triangles, the triple product is equal to d^2.
	float t0 = gSqr(d) / (gSqrt(1.0f + gSqr(d + x0) + gSqr(y0)) * (1.0f + gSqr(x0) + y0 * (d + y0)) +
		gSqrt(1.0f + gSqr(x0) + gSqr(y0)) * (1.0f + x0 * (d + x0) + y0 * (d + y0)) +
		gSqrt(1.0f + gSqr(x0) + gSqr(d + y0)) * (1.0f + x0 * (d + x0) + gSqr(y0)) +
		gSqrt((1.0f + gSqr(x0) + gSqr(y0)) * (1.0f + gSqr(d + x0) + gSqr(y0)) * (1.0f + gSqr(x0) + gSqr(d + y0))));

	float t1 = gSqr(d) / (gSqrt(1.0f + gSqr(x0) + gSqr(d + y0)) * (1.0f + gSqr(d + x0) + y0 * (d + y0)) +
		gSqrt(1.0f + gSqr(d + x0) + gSqr(y0)) * (1.0f + x0 * (d + x0) + gSqr(d + y0)) +
		gSqrt(1.0f + gSqr(d + x0) + gSqr(d + y0)) * (1.0f + x0 * (d + x0) + y0 * (d + y0)) +
		gSqrt((1.0f + gSqr(d + x0) + gSqr(y0)) * (1.0f + gSqr(x0) + gSqr(d + y0)) * (1.0f + gSqr(d + x0) + gSqr(d + y0))));

	return 2.0f * atan(t0) + 2.0f * atan(t1);
}

//////////////////////////////////////////////////////////////////////////////

int gGetCubeSizeFromLevel(const Cube& inCube, int inLevel)
{
	ASSERT(inLevel < inCube.mMips.size());
	int size = inCube.mResolution >> inLevel;
	ASSERT(size > 0);
	return size;
}

//////////////////////////////////////////////////////////////////////////////

void gGetCubeFromScratchImageMip(const ScratchImage& inScratchImage, Cube& outCube)
{
	outCube.mResolution = (int)inScratchImage.GetMetadata().width;

	const int levels = (int)inScratchImage.GetMetadata().mipLevels;
	outCube.mMips.resize(levels);

	for (int level = 0; level < levels; ++level)
	{
		const int size = gGetCubeSizeFromLevel(outCube, level);
		Cube::Mip& mip = outCube.mMips[level];

		for (int face = 0; face < CUBEFACE_COUNT; ++face)
		{
			const Image* image = inScratchImage.GetImage(level, face, 0);
			ASSERT(image != 0);
			ASSERT(image->format == DXGI_FORMAT_R32G32B32A32_FLOAT);
			ASSERT(image->width == size);
			ASSERT(image->height == size);
			ASSERT(image->rowPitch == size * 16);
			ASSERT(image->slicePitch == size * size * 16);
			mip.mFaces[face] = reinterpret_cast<Vec4*>(image->pixels);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

void gMultiplyCubeLevelWithSolidAngle(const Cube& inInputCube, int inInputLevel, const Cube& inPremultipliedCube, int inPremultipliedLevel)
{
	ASSERT(inInputLevel < inInputCube.mMips.size());
	ASSERT(inPremultipliedLevel < inPremultipliedCube.mMips.size());
	const int size = gGetCubeSizeFromLevel(inInputCube, inInputLevel);
	ASSERT(size == gGetCubeSizeFromLevel(inPremultipliedCube, inPremultipliedLevel));
	const Cube::Mip& input_mip = inInputCube.mMips[inInputLevel];
	const Cube::Mip& premultiplied_mip = inPremultipliedCube.mMips[inPremultipliedLevel];

	float delta = 2.0f / size;
	float topleft = -1.0f;

	for (int v = 0; v < size; ++v)
	{
		for (int u = 0; u < size; ++u)
		{
			const float solid_angle = sGetSolidAngle(u * delta + topleft, v * delta + topleft, delta);
			for (int face = 0; face < CUBEFACE_COUNT; ++face)
			{
				Vec4 luminance = input_mip.mFaces[face][v * size + u];
				luminance.SetW(1.0f);
				premultiplied_mip.mFaces[face][v * size + u] = luminance * solid_angle;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

void gNormalizeCubeLevel(const Cube& inInputCube, int inInputLevel, const Cube& inNormalizedCube, int inNormalizedLevel)
{
	ASSERT(inInputLevel < inInputCube.mMips.size());
	ASSERT(inNormalizedLevel < inNormalizedCube.mMips.size());
	int size = gGetCubeSizeFromLevel(inInputCube, inInputLevel);
	ASSERT(size == gGetCubeSizeFromLevel(inNormalizedCube, inNormalizedLevel));
	const Cube::Mip& input_mip = inInputCube.mMips[inInputLevel];
	const Cube::Mip& normalized_mip = inNormalizedCube.mMips[inNormalizedLevel];

	int face_pixel_count = size * size;

	for (int face = 0; face < CUBEFACE_COUNT; ++face)
	{
		const Vec4* input_face = input_mip.mFaces[face];
		Vec4* normalized_face = normalized_mip.mFaces[face];
		for (int i = 0; i < face_pixel_count; ++i)
		{
			normalized_face[i] = input_face[i] * (1.0f / input_face[i].GetW());
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

void gGenerateLowerCubeMipsBoxFilter(const Cube& inCube, int inDestLevelBegin, int inDestLevelEnd)
{
	//NOTE: Currently only supports power-of-two texture sizes

	ASSERT(inDestLevelBegin > 0);

	for (int dst_level = inDestLevelBegin; dst_level < inDestLevelEnd; ++dst_level)
	{
		int dst_size = gGetCubeSizeFromLevel(inCube, dst_level);

		ASSERT(dst_size * 2 == gGetCubeSizeFromLevel(inCube, dst_level - 1));

		for (int face = 0; face < CUBEFACE_COUNT; ++face)
		{
			const Vec4* src_face = inCube.mMips[dst_level - 1].mFaces[face];
			Vec4* dst_face = inCube.mMips[dst_level].mFaces[face];

			for (int dst_v = 0; dst_v < dst_size; ++dst_v)
			{
				for (int dst_u = 0; dst_u < dst_size; ++dst_u)
				{
					int src_index = 4 * dst_size * dst_v + 2 * dst_u;

					dst_face[dst_v * dst_size + dst_u] =
						src_face[src_index] +
						src_face[src_index + 1] +
						src_face[src_index + 2 * dst_size] +
						src_face[src_index + 2 * dst_size + 1];
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

const SH1 gGetCubeSH1(const Cube& inCube, int inLevel)
{
	WeightedSH1 weighted_sh1(0.0f);

	gProcessCubeLevel(inCube, inLevel, [&weighted_sh1](const Vec3& inDirection, Vec4& inValue) 
	{
		gAccumulateWeightedSH1(weighted_sh1, gNormalized(inDirection), inValue);
	});

	SH1 normalized_sh1 = gGetNormalizedWeightedSH1(weighted_sh1);
	SH1 irradiance_sh1 = gGetSH1ConvolvedWithCosineLobe(normalized_sh1, 1.0f / 16);

	return irradiance_sh1;
}

//////////////////////////////////////////////////////////////////////////////

void gDivideCubeBySH1(const Cube& inCube, int inLevel, const SH1& inSH1)
{
	gProcessCubeLevel(inCube, inLevel, [&inSH1](const Vec3& inDirection, Vec4& inValue)
	{
		Vec3 eval = gEvaluateSH1(inSH1, gNormalized(inDirection));
		inValue.SetXYZ(inValue.GetXYZ() / eval);
	});
}

//////////////////////////////////////////////////////////////////////////////

void gGetUVToXYZScaleBias(int inResolution, int inFace, Vec4& outScale, Vec4& outBias)
{
	Vec4 scale = sUVToXYZScalePerFace[inFace] / (float)inResolution;
	outScale = scale;
	outBias = sUVToXYZBiasPerFace[inFace] + Vec4(scale.GetXY(), scale.GetZ() + scale.GetW(), 0.0f) * 0.5f;
}

//////////////////////////////////////////////////////////////////////////////

bool gGetOrientationFromString(const wchar_t* inString, const wchar_t* inStringEnd, Math::Mat2x2& outOrientation)
{
	outOrientation = Mat2x2::Identity();

	for (const wchar_t* ptr = inString; ptr != inStringEnd; ++ptr)
	{
		switch (*ptr)
		{
		case 0:
			return true;

		case 'l':
		case 'L':
			// Rotate 90 degrees left (CCW)
			outOrientation *= Mat2x2(0.0f, 1.0f, -1.0f, 0.0f);
			break;

		case 'r':
		case 'R':
			// Rotate 90 degrees right (CW)
			outOrientation *= Mat2x2(0.0f, -1.0f, 1.0f, 0.0f);
			break;

		case 'h':
		case 'H':
			// Flip horizontally
			outOrientation *= Mat2x2(-1.0f, 0.0f, 0.f, 1.0f);
			break;

		case 'v':
		case 'V':
			// Flip horizontally
			outOrientation *= Mat2x2(1.0f, 0.0f, 0.f, -1.0f);
			break;

		default:
			// Unexpected character
			return false;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////
