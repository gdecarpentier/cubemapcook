// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "Math/Vec.h"
#include <vector>

namespace DirectX
{
class ScratchImage;
} // namespace DirectX

namespace Math
{
struct SH1;
class Mat2x2;
} // namespace Math

//////////////////////////////////////////////////////////////////////////////

enum ECubeFace
{
	CUBEFACE_POS_X,
	CUBEFACE_NEG_X,
	CUBEFACE_POS_Y,
	CUBEFACE_NEG_Y,
	CUBEFACE_POS_Z,
	CUBEFACE_NEG_Z,

	CUBEFACE_COUNT
};

//////////////////////////////////////////////////////////////////////////////

inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_X>, float inU, float inV) { return Math::Vec3(1.0f,	-inV,	-inU	); }
inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_X>, float inU, float inV) { return Math::Vec3(-1.0f, -inV,	inU		); }
inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_Y>, float inU, float inV) { return Math::Vec3(inU,	1.0f,	inV		); }
inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_Y>, float inU, float inV) { return Math::Vec3(inU,	-1.0f,	-inV	); }
inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_Z>, float inU, float inV) { return Math::Vec3(inU,	-inV,	1.0f	); }
inline Math::Vec3 gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_Z>, float inU, float inV) { return Math::Vec3(-inU,	-inV,	-1.0f	); }

//////////////////////////////////////////////////////////////////////////////

struct Cube
{
	Cube() : mResolution(0) { }

	int mResolution;
	struct Mip
	{
		typedef Math::Vec4* Faces[CUBEFACE_COUNT];
		Faces mFaces;
	};

	std::vector<Mip> mMips;
};

//////////////////////////////////////////////////////////////////////////////

int gGetCubeSizeFromLevel(const Cube& inCube, int inLevel);

void gGetCubeFromScratchImageMip(const DirectX::ScratchImage& inScratchImage, Cube& outCube);

void gMultiplyCubeLevelWithSolidAngle(const Cube& inInputCube, int inInputLevel, const Cube& inPremultipliedCube, int inPremultipliedLevel);

void gNormalizeCubeLevel(const Cube& inInputCube, int inInputLevel, const Cube& inNormalizedCube, int inNormalizedLevel);

void gGenerateLowerCubeMipsBoxFilter(const Cube& inCube, int inDestLevelBegin, int inDestLevelEnd);

const Math::SH1 gGetCubeSH1(const Cube& inCube, int inLevel);

void gDivideCubeBySH1(const Cube& inCube, int inLevel, const Math::SH1& inSH1);

void gGetUVToXYZScaleBias(int inResolution, int inFace, Math::Vec4& outScale, Math::Vec4& outBias);

bool gGetOrientationFromString(const wchar_t* inString, const wchar_t* inStringEnd, Math::Mat2x2& outOrientation);

//////////////////////////////////////////////////////////////////////////////

//typedef void CubeProcessFunction(const Vec3& inDirection, Vec4& inValue)

template<typename T>
void gProcessCubeLevel(const Cube& inCube, int inLevel, T inProcess)
{
	ASSERT(inLevel < inCube.mMips.size());
	int size = gGetCubeSizeFromLevel(inCube, inLevel);
	const Cube::Mip& mip = inCube.mMips[inLevel];

	float delta = 2.0f / size;
	float topleft = -1.0f + 0.5f * delta;

	for (int v = 0; v < size; ++v)
	{
		float f_v = delta * v + topleft;
		for (int u = 0; u < size; ++u)
		{
			float f_u = delta * u + topleft;
			int index = v * size + u;

			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_X>(), f_u, f_v), mip.mFaces[CUBEFACE_POS_X][index]);
			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_X>(), f_u, f_v), mip.mFaces[CUBEFACE_NEG_X][index]);
			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_Y>(), f_u, f_v), mip.mFaces[CUBEFACE_POS_Y][index]);
			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_Y>(), f_u, f_v), mip.mFaces[CUBEFACE_NEG_Y][index]);
			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_POS_Z>(), f_u, f_v), mip.mFaces[CUBEFACE_POS_Z][index]);
			inProcess(gGetCubeDirectionFromFaceUV(Utility::IntType<CUBEFACE_NEG_Z>(), f_u, f_v), mip.mFaces[CUBEFACE_NEG_Z][index]);
		}
	}
}
