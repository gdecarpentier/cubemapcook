// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "CubeConversion.h"
#include "Math/Algorithm.h"
#include "Math/Mat.h"
#include "Math/Vec.h"
#include "Utility/Profile.h"
#include "Utility/Log.h"
#include "../ispc_texcomp/ispc_texcomp.h"
#include <assert.h>

using namespace DirectX;
using namespace Math;

//////////////////////////////////////////////////////////////////////////////

static bool CompressCube_BC6H_UF16(DirectX::ScratchImage& inCube, DirectX::ScratchImage& outCube)
{
	ScratchImage uncompressed_cubemap;
	if (inCube.GetMetadata().format == DXGI_FORMAT_R16G16B16A16_FLOAT)
	{
		uncompressed_cubemap = std::move(inCube);
	}
	else
	{
		HRESULT hr = Convert(inCube.GetImages(), inCube.GetImageCount(), inCube.GetMetadata(), DXGI_FORMAT_R16G16B16A16_FLOAT, TEX_FILTER_DEFAULT | TEX_FILTER_FORCE_NON_WIC, 0.5f, uncompressed_cubemap);
		if (FAILED(hr))
		{
			LOG_ERROR(L" FAILED [convert to DXGI_FORMAT_R16G16B16A16_FLOAT] (%x)\n", hr);
			return false;
		}
	}

	bc6h_enc_settings encoder_settings_slow;
	bc6h_enc_settings encoder_settings_veryslow;
	GetProfile_bc6h_slow(&encoder_settings_slow);
	GetProfile_bc6h_veryslow(&encoder_settings_veryslow);

	outCube.Release();
	outCube.InitializeCube(
		DXGI_FORMAT_BC6H_UF16, 
		uncompressed_cubemap.GetMetadata().width, 
		uncompressed_cubemap.GetMetadata().height, 
		1,
		uncompressed_cubemap.GetMetadata().mipLevels);
	assert(uncompressed_cubemap.GetImageCount() == outCube.GetImageCount());

	ScratchImage padded_src;

	for (int i = 0; i < uncompressed_cubemap.GetImageCount(); ++i)
	{
		const Image& src = uncompressed_cubemap.GetImages()[i];
		const Image& tgt = outCube.GetImages()[i];
		assert(src.width == tgt.width && src.height == tgt.height);

		rgba_surface surface = {src.pixels, (int32_t)src.width, (int32_t)src.height, (int32_t)src.rowPitch };

		int padded_width = (src.width + 3) & ~3;
		int padded_height = (src.height + 3) & ~3;
		if (src.width != padded_width || src.height != padded_height)
		{
			padded_src.Release();
			padded_src.Initialize2D(src.format, padded_width, padded_height, 1, 0);
			const Image* padded_image = padded_src.GetImages();
			rgba_surface padded_surface = {padded_image->pixels, (int32_t)padded_image->width, (int32_t)padded_image->height, (int32_t)padded_image->rowPitch};

			int bits_per_pixel = 4 * 16;
			ReplicateBorders(&padded_surface, &surface, 0, 0, bits_per_pixel);
			surface = padded_surface;
		}

		if (src.width < 100)
			CompressBlocksBC6H(&surface, tgt.pixels, &encoder_settings_veryslow);
		else
			CompressBlocksBC6H(&surface, tgt.pixels, &encoder_settings_slow);

	}

	padded_src.Release();

	return true;
}

//////////////////////////////////////////////////////////////////////////////

bool gConvertCube(DirectX::ScratchImage& inCube, DirectX::ScratchImage& outCube, DXGI_FORMAT inFormat)
{
	HRESULT hr;

	if (inCube.GetMetadata().format == inFormat)
	{
		outCube = std::move(inCube);
	}
	else if (!IsCompressed(inFormat))
	{
		hr = Convert(inCube.GetImages(), inCube.GetImageCount(), inCube.GetMetadata(), inFormat, TEX_FILTER_DEFAULT | TEX_FILTER_FORCE_NON_WIC, 0.5f, outCube);
		if (FAILED(hr))
		{
			LOG_ERROR(L" FAILED [convert to output format] (%x)\n", hr);
			return false;
		}

	}
	else if (inFormat == DXGI_FORMAT_BC6H_UF16)
	{
		return CompressCube_BC6H_UF16(inCube, outCube);
	}
	else
	{
		hr = Compress(inCube.GetImages(), inCube.GetImageCount(), inCube.GetMetadata(), inFormat, TEX_COMPRESS_DEFAULT | TEX_COMPRESS_PARALLEL, 0.5f, outCube);
		if (FAILED(hr))
		{
			LOG_ERROR(L" FAILED [compress to output format] (%x)\n", hr);
			return false;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

void gReorientCubeFaces(DirectX::ScratchImage& inCube, const Mat2x2 (&inOrientations)[6])
{
	int resolution = (int)inCube.GetMetadata().width;
	
	ASSERT(inCube.GetMetadata().format == DXGI_FORMAT_R32G32B32A32_FLOAT);
	ASSERT(inCube.GetMetadata().arraySize == 6);
	ASSERT(inCube.GetMetadata().height == resolution);

	const int pixel_pitch = 4 * sizeof(float);
	int max_pitch = resolution * pixel_pitch;

	Vec4* original = (Vec4*)new char[resolution * max_pitch];

	for (int face = 0; face < (int)inCube.GetMetadata().arraySize; ++face)
	{
		const Mat2x2& orientation = inOrientations[face % 6];

		for (int level = 0; level < (int)inCube.GetMetadata().mipLevels; ++level)
		{
			int level_resolution = resolution >> level;
			const Image* image = inCube.GetImage(level, face, 0);
			int row_pitch = (int)image->rowPitch;
			ASSERT(row_pitch <= max_pitch && row_pitch == pixel_pitch * level_resolution);
			if (orientation != Mat2x2::Identity())
			{
				memcpy(original, image->pixels, image->rowPitch * image->height);

				int xx = gRoundToInt(orientation.GetXX()), xy = gRoundToInt(orientation.GetXY());
				int yx = gRoundToInt(orientation.GetYX()), yy = gRoundToInt(orientation.GetYY());

				int src_x_delta = xx + yx * level_resolution;
				int src_y_delta = xy + yy * level_resolution;

				Vec4* src_row = original + (xx < 0 || xy < 0 ? level_resolution - 1 : 0) + (yx < 0 || yy < 0 ? level_resolution - 1 : 0) * level_resolution;
				Vec4* dst_row = (Vec4*)image->pixels;

				for (int y = 0; y < level_resolution; ++y)
				{
					Vec4* src_ptr = src_row;
					Vec4* dst_ptr = dst_row;
					for (int x = 0; x < level_resolution; ++x)
					{
						*dst_ptr = *src_ptr;
						src_ptr += src_x_delta;
						++dst_ptr;
					}
					src_row += src_y_delta;
					dst_row += level_resolution;
				}
			}
		}
	}

	delete [] original;
}
