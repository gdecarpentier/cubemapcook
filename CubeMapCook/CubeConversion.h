// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <dxgiformat.h>
#include "directxtex.h"

namespace Math
{
	class Mat2x2;
}

//////////////////////////////////////////////////////////////////////////////

bool gConvertCube(DirectX::ScratchImage& inCube, DirectX::ScratchImage& outCube, DXGI_FORMAT inFormat);

void gReorientCubeFaces(DirectX::ScratchImage& inCube, const Math::Mat2x2 (&inFaceOrientations)[6]);