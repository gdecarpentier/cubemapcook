// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "CubeIO.h"
#include "Utility/NamedValues.h"
#include "Utility/Formats.h"
#include "Utility/Log.h"
#include "FreeImage.h"
#include "directxtex.h"

#include <vector>
#include <memory>

using namespace DirectX;
using namespace Utility;

//////////////////////////////////////////////////////////////////////////////
static bool LoadImageFromFileUsingFreeImage(const wchar_t* inFilename, ScratchImage& outImage)
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	fif = FreeImage_GetFileTypeU(inFilename, 0);
	if(fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilenameU(inFilename);

	if (fif == FIF_UNKNOWN || !FreeImage_FIFSupportsReading(fif))
	{
		LOG_ERROR(L"Unknown/unsupported filetype for %ls\n", inFilename);
		return false;
	}

	FIBITMAP *bitmap = FreeImage_LoadU(fif, inFilename, 0);
	if (bitmap == nullptr)
	{
		LOG_ERROR(L"Failed to load %ls", inFilename);
		return false;
	}

	if (FreeImage_GetImageType(bitmap) != FIT_RGBAF)
	{
		FIBITMAP* original = bitmap;
		bitmap = FreeImage_ConvertToRGBAF(bitmap);
		FreeImage_Unload(original);
		if (bitmap == nullptr)
		{
			LOG_ERROR(L"Failed to convert %ls", inFilename);
			return false;
		}
	}

	// Fetch dimensions of image
	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);

	// Create new Image with the correct dimenions
	outImage.Release();
	HRESULT hr = outImage.Initialize2D(DXGI_FORMAT_R32G32B32A32_FLOAT, width, height, 1, 1);
	if (FAILED(hr))
	{
		LOG_ERROR(L"Failed to create image (%x)\n", hr);
		FreeImage_Unload(bitmap);
		return false;
	}

	// Copy the converted data up-side-down to the Image
	int src_pitch = FreeImage_GetPitch(bitmap);
	int dst_pitch = 16 * width;
	char* src_ptr = (char*)FreeImage_GetBits(bitmap);
	char* dst_ptr = (char*)outImage.GetImages()->pixels + dst_pitch * (height - 1);
	ASSERT(dst_pitch <= src_pitch && outImage.GetImages()->rowPitch == dst_pitch);
	for (int i = 0; i < height; ++i)
	{
		memcpy(dst_ptr, src_ptr, src_pitch);
		src_ptr += src_pitch;
		dst_ptr -= dst_pitch;
	}

	FreeImage_Unload(bitmap);
		
	return true;
}

//////////////////////////////////////////////////////////////////////////////

bool gReadAndAssembleCube(const std::list<Path>& inFilenames, ScratchImage& outCube, DXGI_FORMAT& outSuggestedOutputFormat)
{
	// Convert images
	size_t images = 0;

	std::list<ScratchImage> loaded_images;

	HRESULT hr = S_OK;

	for (auto in_filename = inFilenames.begin(); in_filename != inFilenames.end(); ++in_filename)
	{
		wchar_t ext[_MAX_EXT];
		wchar_t fname[_MAX_FNAME];
		_wsplitpath_s(*in_filename, nullptr, 0, nullptr, 0, fname, _MAX_FNAME, ext, _MAX_EXT);

		// Load source image
		LOG_DEBUG(L"\nReading %ls%ls...", fname, ext);

		TexMetadata tex_metadata;
		ScratchImage image;

		if (_wcsicmp(ext, L".dds") == 0)
		{
			hr = LoadFromDDSFile(*in_filename, DDS_FLAGS_NONE, &tex_metadata, image);
			if (FAILED(hr))
			{
				LOG_ERROR(L" Failed (%x)\n", hr);
				return false;
			}

			if (tex_metadata.depth > 1)
			{
				LOG_ERROR(L" ERROR: Can't assemble volumes\n");
				return false;
			}

			if (outSuggestedOutputFormat == DXGI_FORMAT_UNKNOWN)
				outSuggestedOutputFormat = tex_metadata.format;
		}
		else
		{
			if (!LoadImageFromFileUsingFreeImage(*in_filename, image))
			{
				LOG_ERROR(L" Failed to load %ls\n", *in_filename);
				return false;
			}

			tex_metadata = image.GetMetadata();
		}

		LOG_DEBUG(L" (%Iux%Iu %ls)\n", tex_metadata.width, tex_metadata.height, gLookupNameByValue(tex_metadata.format, gNamedSupportedFormats));

		// --- Decompress --------------------------------------------------------------
		if (IsCompressed(tex_metadata.format))
		{
			const Image* img = image.GetImage(0, 0, 0);
			assert(img);
			size_t image_count = image.GetImageCount();

			ScratchImage new_image;

			hr = Decompress(img, image_count, tex_metadata, DXGI_FORMAT_UNKNOWN /* picks good default */, new_image);
			if (FAILED(hr))
			{
				LOG_ERROR(L" FAILED [decompress] (%x)\n", hr);
				return false;
			}

			const TexMetadata& metadata = new_image.GetMetadata();

			tex_metadata.format = metadata.format;

			assert(tex_metadata.width == metadata.width);
			assert(tex_metadata.height == metadata.height);
			assert(tex_metadata.depth == metadata.depth);
			assert(tex_metadata.arraySize == metadata.arraySize);
			assert(tex_metadata.mipLevels == metadata.mipLevels);
			assert(tex_metadata.miscFlags == metadata.miscFlags);
			assert(tex_metadata.miscFlags2 == metadata.miscFlags2);
			assert(tex_metadata.dimension == metadata.dimension);

			std::swap(image, new_image);
		}

		images += tex_metadata.arraySize;
		loaded_images.push_back(ScratchImage());
		std::swap(loaded_images.back(), image);
	}

	if (images != 6)
	{
		LOG_ERROR(L" ERROR: requires six images to form the faces of the cubemap\n");
		return false;
	}

	std::vector<Image> image_array;
	image_array.reserve(images);

	for (auto it = loaded_images.cbegin(); it != loaded_images.cend(); ++it)
	{
		const ScratchImage& loaded_image = *it;
		for (size_t j = 0; j < loaded_image.GetMetadata().arraySize; ++j)
		{
			const Image* img = loaded_image.GetImage(0, j, 0);
			ASSERT(img != 0);
			image_array.push_back(*img);
		}
	}

	hr = outCube.InitializeCubeFromImages(&image_array[0], image_array.size());
	if (FAILED(hr))
	{
		LOG_ERROR(L"FAILED building result image (%x)\n", hr);
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

bool gWriteCube(const Path& inFilename, DirectX::ScratchImage& inCube, bool inWriteDDSDX10Header)
{
	// Write texture
	LOG_DEBUG(L"Writing %ls...", inFilename);
	LOG_DEBUG(L" (%Iux%Iux6 %ls)\n", inCube.GetMetadata().width, inCube.GetMetadata().height, gLookupNameByValue(inCube.GetMetadata().format, gNamedSupportedFormats));

	HRESULT hr = SaveToDDSFile(
		inCube.GetImages(),
		inCube.GetImageCount(),
		inCube.GetMetadata(),
		inWriteDDSDX10Header ? (DDS_FLAGS_FORCE_DX10_EXT | DDS_FLAGS_FORCE_DX10_EXT_MISC2) : DDS_FLAGS_NONE,
		inFilename);

	if (FAILED(hr))
	{
		LOG_ERROR(L"\nFAILED (%x)\n", hr);
		return false;
	}

	return true;
}