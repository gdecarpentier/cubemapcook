// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <list>
#include "Utility/Path.h"
#include <dxgiformat.h>
#include "directxtex.h"

//////////////////////////////////////////////////////////////////////////////

bool gReadAndAssembleCube(const std::list<Utility::Path>& inFilenames, DirectX::ScratchImage& outCube, DXGI_FORMAT& outSuggestedOutputFormat);

bool gWriteCube(const Utility::Path& inFilename, DirectX::ScratchImage& inCube, bool inWriteDDSDX10Header);