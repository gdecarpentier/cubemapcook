#pragma once

enum class CPUAccess
{
	Read,
	Write,
	ReadWrite
};