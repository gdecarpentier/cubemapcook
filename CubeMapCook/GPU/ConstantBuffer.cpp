// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "ConstantBuffer.h"
#include "Device.h"

using namespace Utility;

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

ID3D11Buffer* Internal::gCreateD3DConstantBuffer(size_t inBufferSize)
{
	D3D11_BUFFER_DESC buffer_desc;
	buffer_desc.ByteWidth = (UINT)inBufferSize;
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());

	ID3D11Buffer* buffer = 0;
	HRESULT hr = d3d_device.CreateBuffer(&buffer_desc, nullptr, &buffer);
	gVerify(SUCCEEDED(hr), L"Failed to create constant buffer (%08X)", hr);
	ASSERT(buffer != nullptr);

	return buffer;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU