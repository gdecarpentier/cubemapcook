// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include <inttypes.h>

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Context;

namespace Internal {

ID3D11Buffer* gCreateD3DConstantBuffer(size_t inBufferSize);

} // namespace Internal

//////////////////////////////////////////////////////////////////////////////

template<typename CONSTANTS>
class alignas(16) ConstantBuffer : NonCopyable
{
	enum EShadowBufferSize { ShadowBufferSize = (sizeof(CONSTANTS) + 15) & ~15 };

public:
	ConstantBuffer() : 
		mBuffer(0) 
	{
	}

	~ConstantBuffer()
	{ 
		if (mBuffer != nullptr) 
			Finalize();
	}

	void Initialize()
	{
		ASSERT(mBuffer == nullptr);
		mBuffer = GPU::Internal::gCreateD3DConstantBuffer(ShadowBufferSize);
	}

	void Finalize()
	{
		ASSERT(mBuffer != nullptr);
		mBuffer->Release();
		mBuffer = nullptr;
	}

	CONSTANTS& GetConstants() 
	{
		return mShadowBuffer.mConstants;
	}

	const CONSTANTS& GetConstants() const 
	{ 
		return mShadowBuffer.mConstants;
	}

	struct Internal
	{
		static ID3D11Buffer* const GetD3DBuffer(const ConstantBuffer& inConstantBuffer)
		{
			ASSERT(inConstantBuffer.mBuffer != nullptr);
			return inConstantBuffer.mBuffer;
		}
	}; 

private:
	union ShadowBuffer
	{
		ShadowBuffer() { }

		CONSTANTS mConstants;
		uint8_t mRawBytes[ShadowBufferSize];
	};

	ShadowBuffer mShadowBuffer;
	mutable ID3D11Buffer* mBuffer;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU