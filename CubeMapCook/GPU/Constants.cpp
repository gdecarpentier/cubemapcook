// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Constants.h"
#include <d3d11.h>

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

uint32_t Internal::gGetD3DCPUAccessFlag(CPUAccess inCPUAccess)
{
	switch (inCPUAccess)
	{
	case CPUAccess::Read:
		return D3D11_CPU_ACCESS_READ;

	case CPUAccess::Write:
		return D3D11_CPU_ACCESS_WRITE;

	case CPUAccess::ReadWrite:
		return D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU
