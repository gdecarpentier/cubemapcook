// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

typedef Utility::TypedInt<class ConstantBufferSlotTypedInt> ConstantBufferSlot;
typedef Utility::TypedInt<class TextureSlotTypedInt> TextureSlot;
typedef Utility::TypedInt<class SamplerSlotTypedInt> SamplerSlot;
typedef Utility::TypedInt<class UAVSlotTypedInt> UAVSlot;

//////////////////////////////////////////////////////////////////////////////

enum class CPUAccess
{
	None,
	Read,
	Write,
	ReadWrite
};

//////////////////////////////////////////////////////////////////////////////

namespace Internal
{

uint32_t gGetD3DCPUAccessFlag(CPUAccess inCPUAccess);

} // namespace Internal

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU
