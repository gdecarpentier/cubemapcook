// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Math/IVec.h"
#include "Context.h"
#include "Cube.h"
#include "Sampler.h"
#include "Shader.h"
#include "RWTextureView.h"
#include "TextureCube.h"
#include "TextureView.h"

using namespace Math;

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

Context::Context() :
	mD3DContext(nullptr),
	mBoundShaderType(ShaderType::COUNT),
	mBoundTextureSlots(0),
	mBoundUAVSlots(0)
{
}

//////////////////////////////////////////////////////////////////////////////

Context::~Context()
{
	if (mD3DContext != nullptr)
		Finalize();
}

//////////////////////////////////////////////////////////////////////////////

void Context::Initialize(ID3D11DeviceContext* inD3DContext)
{
	ASSERT(mD3DContext == nullptr);
	mD3DContext = inD3DContext;
}

//////////////////////////////////////////////////////////////////////////////

void Context::Finalize()
{
	ASSERT(mD3DContext != nullptr);
	mD3DContext->Release();
	mD3DContext = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

void Context::Bind(const Sampler& inSampler, SamplerSlot inSlot)
{
	ID3D11SamplerState* sampler = Sampler::Internal::GetD3DSampler(inSampler);
	switch (mBoundShaderType)
	{
	case ShaderType::Compute:
		mD3DContext->CSSetSamplers(inSlot.GetValue(), 1, &sampler);
		break;

	default:
		ASSERT(false);
	}
}

void Context::Bind(const Shader& inShader)
{
	UnbindAll();

	mBoundShaderType = inShader.GetShaderType();

	switch (mBoundShaderType)
	{
	case ShaderType::Compute:
		mD3DContext->CSSetShader(Shader::Internal::GetD3DComputeShader(inShader), 0, 0);
		break;

	default:
		ASSERT(false);
	}
}

void Context::Bind(const TextureView* inTextureView, TextureSlot inSlot)
{
	ASSERT(inSlot.GetValue() < sizeof(mBoundTextureSlots) * 8);

	ID3D11ShaderResourceView* srv = nullptr;
	if (inTextureView != nullptr)
	{
		srv = TextureView::Internal::GetD3DSRV(*inTextureView);
		mBoundTextureSlots |= 1 << inSlot.GetValue();
	}

	switch (mBoundShaderType)
	{
	case ShaderType::Compute:
		mD3DContext->CSSetShaderResources(inSlot.GetValue(), 1, &srv);
		break;

	default:
		ASSERT(false);
	}
}

void Context::Bind(const RWTextureView* inRWTextureView, UAVSlot inSlot)
{
	ASSERT(inSlot.GetValue() < sizeof(mBoundUAVSlots) * 8);

	ID3D11UnorderedAccessView* uav = nullptr;
	if (inRWTextureView != nullptr)
	{
		uav = RWTextureView::Internal::GetD3DUAV(*inRWTextureView);
		mBoundUAVSlots |= 1 << inSlot.GetValue();
	}

	switch (mBoundShaderType)
	{
	case ShaderType::Compute:
		mD3DContext->CSSetUnorderedAccessViews(inSlot.GetValue(), 1, &uav, nullptr);
		break;

	default:
		ASSERT(false);
	}
}

//////////////////////////////////////////////////////////////////////////////

void Context::Copy(const Texture2DArray& inSource, const Texture2DArray& inDestination)
{
	Copy(inSource, inDestination, 0, inSource.GetLevels() - 1, 0, inSource.GetSlices() - 1);
}

//////////////////////////////////////////////////////////////////////////////

void Context::Copy(const Texture2DArray& inSource, const Texture2DArray& inDestination, int inFirstLevel, int inLastLevel, int inFirstSlice, int inLastSlice)
{
	ASSERT(inSource.GetWidth() == inDestination.GetWidth() && inSource.GetHeight() == inDestination.GetHeight());
	ASSERT(inFirstLevel >= 0 && inFirstLevel <= inLastLevel && inLastLevel < inSource.GetLevels() && inLastLevel < inDestination.GetLevels());
	ASSERT(inFirstSlice >= 0 && inFirstSlice <= inLastSlice && inFirstSlice < inSource.GetSlices() && inLastSlice < inDestination.GetSlices());

	for (int level = inFirstLevel; level <= inLastLevel; ++level)
	{
		for (int slice = inFirstSlice; slice <= inLastSlice; ++slice)
		{
			mD3DContext->CopySubresourceRegion(
				Texture2DArray::Internal::GetD3DTexture(inDestination),
				level + slice * inDestination.GetLevels(),
				0,
				0,
				0,
				Texture2DArray::Internal::GetD3DTexture(inSource),
				level + slice * inSource.GetLevels(),
				nullptr);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

void Context::Copy(const TextureCube& inSource, const TextureCube& inDestination, int inFirstLevel, int inLastLevel)
{
	Copy(inSource, inDestination, inFirstLevel, inLastLevel, 0, 5);
}

//////////////////////////////////////////////////////////////////////////////

void Context::Dispatch(const IVec3& inGroups)
{
	ASSERT(mBoundShaderType == ShaderType::Compute);
	mD3DContext->Dispatch(inGroups.GetX(), inGroups.GetY(), inGroups.GetZ());
}

void Context::Dispatch(const IVec3& inDispatchThreads, const IVec3& inThreadsPerGroup)
{
	IVec3 groups((inDispatchThreads - IVec3(1)) / inThreadsPerGroup + IVec3(1));
	Dispatch(groups);
}

//////////////////////////////////////////////////////////////////////////////

void Context::Flush()
{
	mD3DContext->Flush();
}

//////////////////////////////////////////////////////////////////////////////

ID3D11DeviceContext* Context::Internal::GetD3DContext(const Context& inContext)
{
	ASSERT(inContext.mD3DContext != nullptr);
	return inContext.mD3DContext;
}

//////////////////////////////////////////////////////////////////////////////

void Context::BindConstantBuffer(ID3D11Buffer* inD3DBuffer, ConstantBufferSlot inSlot)
{
	ID3D11Buffer** pointer = &inD3DBuffer;

	switch (mBoundShaderType)
	{
	case ShaderType::Compute:
		mD3DContext->CSSetConstantBuffers(inSlot.GetValue(), 1, pointer);
		break;

	default:
		ASSERT(false);
	}
}

//////////////////////////////////////////////////////////////////////////////

void Context::UploadConstantBuffer(ID3D11Buffer* inD3DBuffer, const void* inShadowBuffer)
{
	mD3DContext->UpdateSubresource(inD3DBuffer, 0, nullptr, inShadowBuffer, 0, 0);
}

//////////////////////////////////////////////////////////////////////////////

void Context::UnbindAll()
{
	for (int index = 0; mBoundTextureSlots != 0; ++index, mBoundTextureSlots >>= 1)
		if ((mBoundTextureSlots & 1) != 0)
			Bind(nullptr, GPU::TextureSlot(index));

	for (int index = 0; mBoundUAVSlots != 0; ++index, mBoundUAVSlots >>= 1)
		if ((mBoundUAVSlots & 1) != 0)
			Bind(nullptr, GPU::UAVSlot(index));

	mBoundTextureSlots = 0;
	mBoundUAVSlots = 0;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU