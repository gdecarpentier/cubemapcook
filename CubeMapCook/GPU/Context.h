// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include "Constants.h"

struct Cube;

namespace Math
{
class IVec3;
}

namespace GPU {

class Sampler;
class Shader;
class RWTextureView;
class Texture2DArray;
class TextureView;
class TextureCube;
enum class ShaderType;
typedef Utility::TypedInt<class SamplerSlotTypedInt> SamplerSlot;

template<typename CONSTANTS>
class alignas(16) ConstantBuffer;

//////////////////////////////////////////////////////////////////////////////

class Context : Utility::NonCopyable
{
public:
	Context();
	~Context();

	void Initialize(ID3D11DeviceContext* inD3DContext);
	void Finalize();

	void Bind(const Sampler& inSampler, SamplerSlot inSlot);
	void Bind(const Shader& inShader);
	void Bind(const TextureView* inTextureView, TextureSlot inSlot);
	void Bind(const RWTextureView* inRWTextureView, UAVSlot inSlot);

	template<typename CONSTANTS>
	void Bind(const ConstantBuffer<CONSTANTS>& inConstantBuffer, ConstantBufferSlot inSlot)
	{
		BindConstantBuffer(ConstantBuffer<CONSTANTS>::Internal::GetD3DBuffer(inConstantBuffer), inSlot);
	}

	void Copy(const Texture2DArray& inSource, const Texture2DArray& inDestination);
	void Copy(const Texture2DArray& inSource, const Texture2DArray& inDestination, int inFirstLevel, int inLastLevel, int inFirstSlice, int inLastSlice);
	void Copy(const TextureCube& inSource, const TextureCube& inDestination, int inFirstLevel, int inLastLevel);

	void Dispatch(const Math::IVec3& inGroups);
	void Dispatch(const Math::IVec3& inDispatchThreads, const Math::IVec3& inThreadsPerGroup);

	void Flush();

	template<typename CONSTANTS>
	void Upload(const ConstantBuffer<CONSTANTS>& inConstantBuffer)
	{
		UploadConstantBuffer(ConstantBuffer<CONSTANTS>::Internal::GetD3DBuffer(inConstantBuffer), &inConstantBuffer.GetConstants());
	}

	struct Internal
	{
		static ID3D11DeviceContext* GetD3DContext(const Context& inContext);
	};

private:
	void BindConstantBuffer(ID3D11Buffer* inD3DBuffer, ConstantBufferSlot inSlot);
	void UploadConstantBuffer(ID3D11Buffer* inD3DBuffer, const void* inShadowBuffer);
	void UnbindAll();

private:
	ID3D11DeviceContext* mD3DContext;
	ShaderType mBoundShaderType;
	uint32_t mBoundTextureSlots;
	uint32_t mBoundUAVSlots;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU