// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Device.h"
#include "Utility/Log.h"

using namespace Utility;

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

namespace
{
	static Device gDevice;
} // namespace

//////////////////////////////////////////////////////////////////////////////

Device::Device() :
	mD3DDevice(nullptr)
{
}

//////////////////////////////////////////////////////////////////////////////

Device::~Device()
{
	if (mD3DDevice != nullptr)
		Finalize();
}

//////////////////////////////////////////////////////////////////////////////

void Device::Initialize()
{
	ASSERT(mD3DDevice == nullptr);

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	ID3D11DeviceContext* d3dcontext;

	const D3D_FEATURE_LEVEL feauture_levels[] = { D3D_FEATURE_LEVEL_11_0 };
	HRESULT hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags, feauture_levels, 1,
		D3D11_SDK_VERSION, &mD3DDevice, nullptr, &d3dcontext);

	gVerify(SUCCEEDED(hr), L"Failed creating Direct3D 11 device (%08X)\n", hr);
	assert(mD3DDevice && mD3DDevice->GetFeatureLevel() >= D3D_FEATURE_LEVEL_11_0);

	gDevice.GetImmediateContext().Initialize(d3dcontext);
}

//////////////////////////////////////////////////////////////////////////////

void Device::Finalize()
{
	ASSERT(mD3DDevice != nullptr);
	mContext.Finalize();

	mD3DDevice->Release();
	mD3DDevice = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

ID3D11Device* Device::Internal::GetD3DDevice(const Device& inDevice)
{
	ASSERT(inDevice.mD3DDevice != nullptr);
	return inDevice.mD3DDevice;
}

//////////////////////////////////////////////////////////////////////////////

Device& gGetDevice()
{
	return gDevice;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU