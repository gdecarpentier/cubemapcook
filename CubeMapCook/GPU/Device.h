// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "Context.h"

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Device : Utility::NonCopyable
{
public:
	inline Device();
	inline ~Device();

	void Initialize();
	void Finalize();

	Context& GetImmediateContext()
	{
		return mContext;
	}
	
	struct Internal
	{
		static ID3D11Device* GetD3DDevice(const Device& inDevice);
	};

private:
	ID3D11Device* mD3DDevice;
	Context mContext;
};

//////////////////////////////////////////////////////////////////////////////

Device& gGetDevice();

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU