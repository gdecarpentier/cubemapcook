// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "RWTextureView.h"
#include "Texture2D.h"
#include "Texture2DArray.h"
#include "TextureCube.h"
#include "Device.h"

using namespace Math;
using namespace Utility;

namespace GPU
{

//////////////////////////////////////////////////////////////////////////////

RWTextureView::RWTextureView() :	
	mD3DUAV(nullptr)
{
}

//////////////////////////////////////////////////////////////////////////////

RWTextureView::~RWTextureView()
{
	if (mD3DUAV != nullptr)
		Finalize();
}

//////////////////////////////////////////////////////////////////////////////

void RWTextureView::Initialize(const Texture2D& inTexture2D, int inLevel)
{
	ASSERT(mD3DUAV == nullptr);
	ASSERT(inLevel < inTexture2D.GetLevels());

	D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
	uav_desc.Format = (DXGI_FORMAT)0;
	uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	uav_desc.Texture2D.MipSlice = inLevel;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	ID3D11Texture2D* d3d_texture = Texture2DArray::Internal::GetD3DTexture(inTexture2D);
	HRESULT hr = d3d_device.CreateUnorderedAccessView(d3d_texture, &uav_desc, &mD3DUAV);
	gVerify(SUCCEEDED(hr), L"Failed to create UAV for Texture2DArray (%08X)\n", hr);
	ASSERT(mD3DUAV != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void RWTextureView::Initialize(const Texture2DArray& inTexture2DArray, int inLevel, int inFirstSlice, int inLastSlice)
{
	ASSERT(mD3DUAV == nullptr);
	ASSERT(inFirstSlice <= inLastSlice);
	ASSERT(inLevel < inTexture2DArray.GetLevels());
	ASSERT(inLastSlice < inTexture2DArray.GetSlices());

	D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
	uav_desc.Format = (DXGI_FORMAT)0;
	uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
	uav_desc.Texture2DArray.MipSlice = inLevel;
	uav_desc.Texture2DArray.FirstArraySlice = inFirstSlice;
	uav_desc.Texture2DArray.ArraySize = 1 + inLastSlice - inFirstSlice;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	ID3D11Texture2D* d3d_texture = Texture2DArray::Internal::GetD3DTexture(inTexture2DArray);
	HRESULT hr = d3d_device.CreateUnorderedAccessView(d3d_texture, &uav_desc, &mD3DUAV);
	gVerify(SUCCEEDED(hr), L"Failed to create UAV for Texture2DArray (%08X)\n", hr);
	ASSERT(mD3DUAV != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void RWTextureView::Initialize(const TextureCube& inTextureCube, int inLevel)
{
	Initialize(inTextureCube, inLevel, 0, 5);
}

//////////////////////////////////////////////////////////////////////////////

void RWTextureView::Finalize()
{
	ASSERT(mD3DUAV != nullptr);
	mD3DUAV->Release();
	mD3DUAV = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

ID3D11UnorderedAccessView* RWTextureView::Internal::GetD3DUAV(const RWTextureView& inRWTextureView)
{
	ASSERT(inRWTextureView.mD3DUAV != nullptr);
	return inRWTextureView.mD3DUAV;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU