// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include "Constants.h"

namespace GPU {

class Texture2D;
class Texture2DArray;
class TextureCube;

//////////////////////////////////////////////////////////////////////////////

class RWTextureView
{
public:
	RWTextureView();
	~RWTextureView();

	void Initialize(const Texture2D& inTexture2D, int inLevel);
	void Initialize(const Texture2DArray& inTexture2DArray, int inLevel, int inFirstSlice, int inLastSlice);
	void Initialize(const TextureCube& inTextureCube, int inLevel);
	void Finalize();

	struct Internal
	{
		static ID3D11UnorderedAccessView* GetD3DUAV(const RWTextureView& inRWTextureView);
	};

private:
	ID3D11UnorderedAccessView* mD3DUAV;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU