// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Device.h"
#include "Sampler.h"

using namespace Utility;

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

Sampler::Sampler() :
	mD3DSampler(nullptr)
{
}

//////////////////////////////////////////////////////////////////////////////

Sampler::~Sampler()
{
	if (mD3DSampler != nullptr)
		Finalize();
}

//////////////////////////////////////////////////////////////////////////////

void Sampler::Initialize(TextureFilter inTextureFilter, TextureAddress inTextureAddress, int inMinLevel, int inMaxLevel, float inLevelBias, const Math::Vec4& inBorderColor)
{
    D3D11_SAMPLER_DESC sampler_desc;

	switch (inTextureFilter)
	{
	case TextureFilter::Point:			sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;			break;
	case TextureFilter::Bilinear:		sampler_desc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;	break;
	case TextureFilter::Trilinear:		sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;			break;
	default:							ASSERT(false);
	}

	D3D11_TEXTURE_ADDRESS_MODE address = (D3D11_TEXTURE_ADDRESS_MODE)0;
	switch (inTextureAddress)
	{
	case TextureAddress::Wrap:			address = D3D11_TEXTURE_ADDRESS_WRAP;			break;
	case TextureAddress::Mirror:		address = D3D11_TEXTURE_ADDRESS_MIRROR;			break;
	case TextureAddress::Clamp:			address = D3D11_TEXTURE_ADDRESS_CLAMP;			break;
	case TextureAddress::Border:		address = D3D11_TEXTURE_ADDRESS_BORDER;			break;
	case TextureAddress::MirrorOnce:	address = D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;	break;
	default:							ASSERT(false);
	}

    sampler_desc.AddressU = address;
    sampler_desc.AddressV = address;
    sampler_desc.AddressW = address;
	sampler_desc.MipLODBias = inLevelBias;
	sampler_desc.MaxAnisotropy = 0;
    sampler_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampler_desc.BorderColor[0] = inBorderColor.GetX();
	sampler_desc.BorderColor[1] = inBorderColor.GetY();
	sampler_desc.BorderColor[2] = inBorderColor.GetZ();
	sampler_desc.BorderColor[3] = inBorderColor.GetW();
    sampler_desc.MinLOD = (float)inMinLevel;
    sampler_desc.MaxLOD = (float)inMaxLevel;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	HRESULT hr = d3d_device.CreateSamplerState(&sampler_desc, &mD3DSampler);
	gVerify(SUCCEEDED(hr) && mD3DSampler != nullptr, L"Failed to create sampler (%08X)", hr);
	ASSERT(mD3DSampler != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void Sampler::Finalize()
{
	ASSERT(mD3DSampler != nullptr);
	mD3DSampler->Release();
}

//////////////////////////////////////////////////////////////////////////////

ID3D11SamplerState* Sampler::Internal::GetD3DSampler(const Sampler& inSampler)
{
	ASSERT(inSampler.mD3DSampler != nullptr);
	return inSampler.mD3DSampler;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU

