// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include "Math/Vec.h"

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

enum class TextureFilter
{
	Point,
	Bilinear,
	Trilinear,
};

//////////////////////////////////////////////////////////////////////////////

enum class TextureAddress
{
	Wrap,
	Mirror,
	Clamp,
	Border,
	MirrorOnce,
};

//////////////////////////////////////////////////////////////////////////////

class Sampler : Utility::NonCopyable
{
public:
	Sampler();
	~Sampler();

	void Initialize(TextureFilter inTextureFilter, TextureAddress inTextureAddress, int inMinLevel = 0, int inMaxLevel = 0x7FFFFFFF, float inLevelBias = 0.0f, const Math::Vec4& inBorderColor = Math::Vec4(0.0f));
	void Finalize();

	struct Internal
	{
		static ID3D11SamplerState* GetD3DSampler(const Sampler& inSampler);
	};

private:
	ID3D11SamplerState* mD3DSampler;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU