// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Shader.h"
#include "Device.h"

using namespace Utility;

//////////////////////////////////////////////////////////////////////////////

#define CREATE_COMPUTE_SHADER(inName) \
	sCreateComputeShader(PrecompiledShader::inName, #inName, gByteCode_##inName, sizeof(gByteCode_##inName));

//////////////////////////////////////////////////////////////////////////////

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

namespace
{
	#include "Shaders/BilinearDownscale.h"
	#include "Shaders/ImportanceGGX.h"
	#include "Shaders/ColorConvert.h"
	#include "Shaders/CubeToLongLat.h"
	#include "Shaders/MultiplySolidAngle.h"
	#include "Shaders/AmbientBRDF.h"

	static Shader sShaders[PrecompiledShader::COUNT];
} // namespace

//////////////////////////////////////////////////////////////////////////////

Shader::Shader() :
	mShaderType(ShaderType::COUNT)
{
}

//////////////////////////////////////////////////////////////////////////////

Shader::~Shader()
{
	ASSERT(mShaderType == ShaderType::COUNT);
}

//////////////////////////////////////////////////////////////////////////////

void Shader::Initialize(ID3D11ComputeShader* inD3DComputeShader)
{
	ASSERT(mShaderType == ShaderType::COUNT);
	mShaderType = ShaderType::Compute;
	mD3DShader.mComputeShader = inD3DComputeShader;
}

//////////////////////////////////////////////////////////////////////////////

void Shader::Finalize()
{
	switch (mShaderType)
	{
	case ShaderType::Compute:
		ASSERT(mD3DShader.mComputeShader != nullptr);
		mD3DShader.mComputeShader->Release();
		mD3DShader.mComputeShader = nullptr;
		break;
	default:
		ASSERT(false);
	}

	mShaderType = ShaderType::COUNT;
}

//////////////////////////////////////////////////////////////////////////////

ID3D11ComputeShader* Shader::Internal::GetD3DComputeShader(const Shader& inShader)
{
	ASSERT(inShader.mD3DShader.mComputeShader != nullptr);
	return inShader.mD3DShader.mComputeShader;
}

//////////////////////////////////////////////////////////////////////////////

void sCreateComputeShader(PrecompiledShader inShader, const char* inName, const BYTE* inByteCode, size_t inByteCodeLength)
{
	ID3D11ComputeShader* shader = nullptr;
	ID3D11Device* device = Device::Internal::GetD3DDevice(gGetDevice());
	HRESULT hr = device->CreateComputeShader(inByteCode, inByteCodeLength, nullptr, &shader);
	gVerify(SUCCEEDED(hr), L"Failed to load computer shader %d: %s.", inShader, inName);
	ASSERT(shader != nullptr);

	sShaders[(int)inShader].Initialize(shader);
}	

//////////////////////////////////////////////////////////////////////////////

void gInitializePrecompiledShaders()
{
	CREATE_COMPUTE_SHADER(BilinearDownscale);
	CREATE_COMPUTE_SHADER(ImportanceGGX);
	CREATE_COMPUTE_SHADER(ColorConvert);
	CREATE_COMPUTE_SHADER(CubeToLongLat);
	CREATE_COMPUTE_SHADER(MultiplySolidAngle);
	CREATE_COMPUTE_SHADER(AmbientBRDF);
}

//////////////////////////////////////////////////////////////////////////////

void gFinalizePrecompiledShaders()
{
	for (size_t i = 0; i < (int)PrecompiledShader::COUNT; ++i)
	{
		sShaders[i].Finalize();
	}
}

//////////////////////////////////////////////////////////////////////////////

const Shader& gGetPrecompiledShader(PrecompiledShader inShader)
{
	ASSERT(inShader < PrecompiledShader::COUNT);
	return sShaders[(int)inShader];
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU