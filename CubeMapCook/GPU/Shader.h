// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Context;

//////////////////////////////////////////////////////////////////////////////

enum class ShaderType
{
	Compute,
	COUNT
};

//////////////////////////////////////////////////////////////////////////////

enum class PrecompiledShader
{
	BilinearDownscale,
	ImportanceGGX,
	ColorConvert,
	CubeToLongLat,
	MultiplySolidAngle,
	AmbientBRDF,
	COUNT
};

//////////////////////////////////////////////////////////////////////////////

class Shader : Utility::NonCopyable
{
public:
	inline Shader();
	inline ~Shader();

	void Initialize(ID3D11ComputeShader* inD3DComputeShader);
	void Finalize();

	ShaderType GetShaderType() const 
	{ 
		return mShaderType; 
	}

	struct Internal
	{
		static ID3D11ComputeShader* GetD3DComputeShader(const Shader& inShader);
	};

private:
	union D3DShader
	{
		ID3D11ComputeShader* mComputeShader;
	};

	D3DShader mD3DShader;
	ShaderType mShaderType;
};

//////////////////////////////////////////////////////////////////////////////

void gInitializePrecompiledShaders();
void gFinalizePrecompiledShaders();

//////////////////////////////////////////////////////////////////////////////

const Shader& gGetPrecompiledShader(PrecompiledShader inShader);

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU