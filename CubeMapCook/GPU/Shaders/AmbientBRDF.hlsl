// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "Math.h.hlsl"

Texture2D<float4>			uvTexture			: register(t0);

RWTexture2D<float4>			rwOutput			: register(u0);

cbuffer						constBuffer			: register(b0)
{
	const int2				cDispatchThreadIDOffset;
	const float				cOutputScale;
}

// Sample the GGX function such that the NDF sample is result.x^2 / (PI*(result.y)^2)
float2 GetGGXSqrtNumDenomNDF(float inAlpha, float inCos)
{
	float denom = inCos * inCos * (inAlpha * inAlpha - 1.0) + 1.0;
	return float2(inAlpha, denom);
}

// NOTE: inUniform is expected to be in the range [0, 1), as 1.0 will result in a NaN
float GetGGXImportanceSampleTan(float inUniform, float inAlpha)
{
	return inAlpha * inUniform * rsqrt(-inUniform * inUniform + inUniform);
}

float GetSimpliedGeometryTerm(float inK, float inDot)
{
	return inDot / (inDot * (1 - inK) + inK);
}

float GetFullGeometryLambda(float inAlpha, float inDot)
{
	return -0.5 + 0.5 * sqrt(1.0 + inAlpha * inAlpha * (1.0 / (inDot * inDot) - 1.0));
}

#define USE_IMPORTANCE

#ifdef USE_IMPORTANCE

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint2 uv = inDispatchThreadID.xy + cDispatchThreadIDOffset;
	float2 alpha_cosine = uvTexture.Load(int3(uv, 0)).rg;
	float alpha = alpha_cosine.x;
	float cosine = alpha_cosine.y;

	float3 n = float3(0, 0, 1);
	float3 v = float3(sqrt(1-cosine*cosine), 0, cosine);

	float2 sum = (float2)0.0;
	float samples = 250000;

	float start_angle = GOLDEN_ANGLE * GetSimpleIntHash2D(inDispatchThreadID.xy);
	float2 cos_sin_phi = float2(cos(start_angle), sin(start_angle));
	float2 cos_sin_phi_step = float2(cos(GOLDEN_ANGLE), sin(GOLDEN_ANGLE));
	float sample_step = 1.0 / samples;

	for (int k = 0; k < samples; ++k)
	{
		float i = (k + 0.5) * sample_step; 
		// Get the tan(theta) for the current importance-sampled sample, where theta is the angle between the normal and the half vector,
		// And the single and double angle cos-sin pairs from that.
		float tan_theta = GetGGXImportanceSampleTan(i, alpha);
		float cos_theta = rsqrt(tan_theta * tan_theta + 1.0);
		float sin_theta = tan_theta * cos_theta;
		
		float3 h = float3(sin_theta * cos_sin_phi, cos_theta);
		float3 l = -reflect(v, h);
		float nol = saturate(l.z), nov = saturate(v.z), noh = saturate(h.z), voh = saturate(dot(v, h));

		if (nol > 0)
		{
			// The importance samples are generated in a NDF*dot(n,h) PDF of half vectors, so compensate for the dot(n,h) and the difference 
			// rate in solid angle when going from halfvector angles to (doubled) light angles. 
			// The latter is (jacobian_relative_area) * sin(ang(v,l))/sin(ang(v,h)) = (2) * sin(2*ang(v,h))/sin(ang(v,h)) = 4 * cos(ang(v,h)).
			// See Eq 16 and 17 of "Notes on the Ward BRDF", Walter, 2005 for more details (although eq 17 seems to be inversed..).
			float distribution_correction_ggx = 1.0 / noh;
			float distribution_correction_h_to_l = 4.0 * voh; 
			float distribution_corrections = distribution_correction_ggx * distribution_correction_h_to_l;

			float light_proj = nol;

			float fresnel_term = pow(1.0 - voh, 5.0); 
			float2 fresnel = float2(1.0 - fresnel_term, fresnel_term);
				
			//float geometry = GetSimpliedGeometryTerm(alpha * 0.5, nov) * GetSimpliedGeometryTerm(alpha * 0.5, nol); // schlick without hotness remapping
			//float geometry = 1.0 / (1.0 + GetFullGeometryLambda(alpha, nov)) / (1.0 + GetFullGeometryLambda(alpha, nol)); // height uncorrelated
			float geometry = 1.0 / (1.0 + GetFullGeometryLambda(alpha, nov) + GetFullGeometryLambda(alpha, nol)); // height correlated

			float normalization = 1.0 / (4.0 * nol * nov);

			sum += float2(light_proj * geometry * fresnel * normalization * distribution_corrections);
		}

		cos_sin_phi = ComplexMultiply(cos_sin_phi, cos_sin_phi_step);
	}

	rwOutput[uv] = float4(cOutputScale * sum / samples, 0.0, 1.0);
}

#else

float3 gGetUniformSpherePoint(int inIndex, int inPoints)
{
	float z = 1.0 - (2.0 * inIndex + 0.5) / (float)inPoints;
	float r = sqrt(1 - z * z);
	float ang = 3.88322 * (float)(inIndex & 1023);
	return float3(z, cos(ang) * r, sin(ang) * r);
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint2 uv = inDispatchThreadID.xy + cDispatchThreadIDOffset;
	float2 alpha_cosine = uvTexture.Load(int3(uv, 0)).rg;
	float alpha = alpha_cosine.x;
	float cosine = alpha_cosine.y;

	float3 n = float3(0, 0, 1);
	float3 v = float3(sqrt(1-cosine*cosine), 0, cosine);

	float2 sum = (float2)0.0;
	int samples = 10000000;

	float light_solid_angle = 4 * PI / samples;

	for (int i = 0; i < samples; ++i)
	{
		float3 l = gGetUniformSpherePoint(i, samples);
		float3 h = normalize(v + l);
		float nol = saturate(l.z), nov = saturate(v.z), noh = saturate(h.z), voh = saturate(dot(v, h));

		if (nol > 0)
		{
			float2 ggx = GetGGXSqrtNumDenomNDF(alpha, noh);
			float distribution = ggx.x * ggx.x / (PI * ggx.y * ggx.y);

			float light_proj = nol;

			float fresnel_term = pow(1.0 - voh, 5.0);
			float2 fresnel = float2(1.0 - fresnel_term, fresnel_term);
				
			//float geometry = GetSimpliedGeometryTerm(alpha * 0.5, nov) * GetSimpliedGeometryTerm(alpha * 0.5, nol); // schlick without hotness remapping
			//float geometry = 1.0 / (1.0 + GetFullGeometryLambda(alpha, nov)) / (1.0 + GetFullGeometryLambda(alpha, nol)); // height uncorrelated
			float geometry = 1.0 / (1.0 + GetFullGeometryLambda(alpha, nov) + GetFullGeometryLambda(alpha, nol)); // height correlated

			float normalization = 4.0 * nol * nov;

			sum += light_solid_angle * distribution * light_proj * geometry * fresnel / normalization;
		}
	}
	
	rwOutput[uint3(uv, 0)] = float4(cOutputScale * sum, 0.0, 1.0);
}

#endif