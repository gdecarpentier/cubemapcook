// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

Texture2DArray<float4>		texInput			: register(t0);

RWTexture2DArray<float4>	rwOutput			: register(u0);

SamplerState				sampLinear			: register(s0);

cbuffer						constBuffer			: register(b0)
{
	const float				cRcpOutputResolution;
	const float				cInputLevel;
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint3 uvf = inDispatchThreadID;
	float2 uv_norm = ((float2)uvf.xy + (float2)0.5) * cRcpOutputResolution;
	float4 color = texInput.SampleLevel(sampLinear, float3(uv_norm, uvf.z), cInputLevel);
	rwOutput[uvf] = color;
}


