// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

Texture2DArray<float4>		texInput			: register(t0);

RWTexture2DArray<float4>	rwOutput			: register(u0);

cbuffer						constBuffer			: register(b0)
{
	const float				cLinearScale;
	const float				cLinearThreshold;
	const float				cGammaPreBias;
	const float				cGammaPower;
	const float				cGammaPostScale;
	const float				cGammaPostBias;
}

float ConvertColorComponent(float inColor)
{
	if (inColor < cLinearThreshold)
		return max(inColor * cLinearScale, 0.0);

	return pow(max(inColor + cGammaPreBias, 0.0), cGammaPower) * cGammaPostScale + cGammaPostBias;
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint3 uvf = inDispatchThreadID;
	float3 color = texInput.Load(int4(uvf, 0)).rgb;

	color.r = ConvertColorComponent(color.r);
	color.g = ConvertColorComponent(color.g);
	color.b = ConvertColorComponent(color.b);

	rwOutput[uvf] = float4(color, 1.0);
}
