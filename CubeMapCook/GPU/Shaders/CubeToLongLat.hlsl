// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.
#include "Math.h.hlsl"

TextureCube<float4>			texInput			: register(t0);

RWTexture2D<float4>			rwOutput			: register(u0);

SamplerState				sampLinear			: register(s0);

cbuffer						constBuffer			: register(b0)
{
	const float2			cRcpOutputResolution;
	const float				cInputLevel;
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	float2 uv_norm = ((float2)inDispatchThreadID.xy + (float2)0.5) * cRcpOutputResolution;
	float longitude = (uv_norm.x - 0.5) * (2.0 * PI);
	float lattitude = (uv_norm.y - 0.5) * PI;
	float c = cos(lattitude);
	float3 normal = float3(c * sin(longitude), sin(lattitude), c * cos(longitude));
	float4 color = texInput.SampleLevel(sampLinear, normal, cInputLevel);
	rwOutput[inDispatchThreadID.xy] = color;
}


