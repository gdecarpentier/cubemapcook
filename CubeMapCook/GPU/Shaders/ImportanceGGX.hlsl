// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "Math.h.hlsl"

TextureCube<float4>			texBox			: register(t0);

TextureCube<float4>			texNextLevel	: register(t1);

RWTexture2DArray<float4>	rwOutput		: register(u0);

SamplerState				sampLinear		: register(s0);

cbuffer						constBuffer		: register(b0)
{
	const float4			cXYZCoordScale;
	const float4			cXYZCoordBias;
	const int2				cDispatchThreadIDOffset;
	const float				cAlpha;
	const float				cLevel;
	const float				cNextLevelAlpha;
	const float				cNextLevelWeight;
}

// Sample the GGX function such that the NDF sample is result.x^2 / (PI*(result.y)^2)
float2 GetGGXSqrtNumDenomNDF(float inAlpha, float inCos)
{
	float denom = inCos * inCos * (inAlpha * inAlpha - 1.0) + 1.0;
	return float2(inAlpha, denom);
}

// NOTE: inUniform is expected to be in the range [0, 1), as 1.0 will result in a NaN
float GetGGXImportanceSampleTan(float inUniform, float inAlpha)
{
	return inAlpha * inUniform * rsqrt(-inUniform * inUniform + inUniform);
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint2 uv = inDispatchThreadID.xy + cDispatchThreadIDOffset;

	// Convert the non-normalized uv coordinate to an unnormalized normal direction.
	float3 unnormalized_normal = float3(cXYZCoordScale.xy * (float2)uv, dot(cXYZCoordScale.zw, (float2)uv)) + cXYZCoordBias.xyz;

	// Derive an arbitrary orthonormal basis from the unnormalized normal.
	float3x3 tangent_basis = GetOrthonormalBasis(unnormalized_normal);

	float4 color_sum = (float4)0.0;
	float samples = 2500;

	float start_angle = GOLDEN_ANGLE * GetSimpleIntHash2D(inDispatchThreadID.xy);
	float2 cos_sin_phi = float2(cos(start_angle), sin(start_angle));
	float2 cos_sin_phi_step = float2(cos(GOLDEN_ANGLE), sin(GOLDEN_ANGLE));
	float sample_step = 1.0 / samples;

	for (float i = 0.5 * sample_step; i < 1.0; i += sample_step)
	{
		// Get the tan(theta) for the current importance-sampled sample, where theta is the angle between the normal and the half vector,
		// And the single and double angle cos-sin pairs from that.
		float tan_theta = GetGGXImportanceSampleTan(i, cNextLevelAlpha);
		float cos_theta = rsqrt(tan_theta * tan_theta + 1.0);
		float sin_theta = tan_theta * cos_theta;
		float cos_two_theta = cos_theta * cos_theta - sin_theta * sin_theta;
		float sin_two_theta = 2.0 * sin_theta * cos_theta;

		// Get [sin(2 theta), 0, cos(2 theta)] and rotate that around Z using the [cos(phi), sin(phi)] pair.
		float3 tangent_light_vec = float3(sin_two_theta * cos_sin_phi, cos_two_theta);

		// Convert from tangent space to world space.
		float3 world_light_vec = mul(tangent_basis, tangent_light_vec);

		// Base the mipmap on the relative solid angle of the sample's texel (i.e. the jacobian), only sampling texels with a solid angle 
		// that is smaller or equal to the the smallest texel in the output level.
		float max_coord = max(abs(world_light_vec.x), max(abs(world_light_vec.y), abs(world_light_vec.z)));
		float rcp_jacobian = max_coord * max_coord * max_coord;
		float sample_level = cLevel - 0.5 * log2(rcp_jacobian * (3.0 * SQRT_3));

		// Sample color from the world position. This sample will be a mix of up to four texels, each with a different (solid angle) weight
		// in the alpha channel. 
		float4 color = texBox.SampleLevel(sampLinear, world_light_vec, sample_level);

		// Get the unbiased weight, which is the ideal pdf divided by the importance pdf. Lastly, as we will also blend in 
		// the GGX from the next mip level, reduce the unbiased weight by the next level's blend weight as well.
		float2 importance_weight_num_denom = GetGGXSqrtNumDenomNDF(cNextLevelAlpha, cos_theta);
		float2 pdf_weight_num_denom = GetGGXSqrtNumDenomNDF(cAlpha, cos_theta);
		float2 unbiased_weight_num_denom = pdf_weight_num_denom.xy * importance_weight_num_denom.yx;
		unbiased_weight_num_denom *= unbiased_weight_num_denom;
		unbiased_weight_num_denom.x -= cNextLevelWeight * unbiased_weight_num_denom.y;

		// Integrate the sample, using the 'N dot L' cos(theta) as an additional weight, which will be normalized again after the summation.
		unbiased_weight_num_denom.x *= saturate(cos_two_theta);
		unbiased_weight_num_denom.y *= color.a;
		color_sum += (unbiased_weight_num_denom.x / unbiased_weight_num_denom.y) * color;

		// Rotate the cos-sin pair for the next sample
		cos_sin_phi = ComplexMultiply(cos_sin_phi, cos_sin_phi_step);
	}

	// Re-normalize the result
	float3 final = color_sum.rgb / color_sum.a;

	// Add the part of the integral that was left out in unbiased_weight_num_denom by looking that up in the next mipmap level of the result
	// Because the part that was left in was normalized, use a lerp instead of an add.
	float3 next_level_color = texNextLevel.SampleLevel(sampLinear, unnormalized_normal, 0.0).rgb;
	final = max(lerp(final, next_level_color, cNextLevelWeight), (float3)0.0);

	// Output the result
	uint face = (uint)cXYZCoordBias.w;
	rwOutput[uint3(uv, face)] = float4(final, 1.0);
}


