// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#define PI					3.1415926535
#define SQRT_3				1.7320508076
#define GOLDEN_RATIO		0.6180339887
#define GOLDEN_ANGLE		(2.0*PI*GOLDEN_RATIO)

float3x3 GetOrthonormalBasis(float3 inDirection)
{
	float3 normal = normalize(inDirection);
	float signz = normal.z < 0.0 ? -1.0 : 1.0;
	float scale = 1.0 / (signz + normal.z);
	float3 tangent = float3(normal.x * normal.x * scale - signz, normal.x * normal.y * scale, normal.x);
	float3 bitangent = float3(tangent.y, normal.y * normal.y * scale - signz, normal.y);
	return float3x3(tangent, bitangent, normal);
}

float GetSimpleIntHash2D(uint2 inXY)
{
	return frac(sin(dot(inXY.xy, float2(12.9898, 78.233)))* 43758.5453);
}

float2 ComplexMultiply(float2 inA, float2 inB)
{
	return float2(inA.x * inB.x - inA.y * inB.y,
		          inA.x * inB.y + inA.y * inB.x);
}
