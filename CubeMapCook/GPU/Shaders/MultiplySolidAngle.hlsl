// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

Texture2DArray<float4>		texInput			: register(t0);

RWTexture2DArray<float4>	rwOutput			: register(u0);

cbuffer						constBuffer			: register(b0)
{
	const float				cRcpHalfResolution;
}

[numthreads(8, 8, 1)]
void main(uint3 inDispatchThreadID : SV_DispatchThreadID)
{
	uint3 uvf = inDispatchThreadID;
	float3 color = texInput.Load(int4(uvf, 0)).rgb;

	float2 uv_norm = ((float2)uvf.xy + (float2)0.5) * (cRcpHalfResolution) - (float2)1.0;
	float inv_dist = rsqrt(dot(uv_norm, uv_norm) + 1.0);
	float solid_angle_weight = inv_dist * inv_dist * inv_dist;
	color *= solid_angle_weight;

	rwOutput[uvf] = float4(color, solid_angle_weight);
}


