// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Texture2D.h"
#include "Device.h"

using namespace Math;
using namespace Utility;

namespace GPU
{

//////////////////////////////////////////////////////////////////////////////

void Texture2D::Initialize(int inWidth, int inHeight, int inLevels, DXGI_FORMAT inFormat, CPUAccess inCPUAccess)
{	
	Texture2DArray::Initialize(inWidth, inHeight, inLevels, 1, inFormat, inCPUAccess, 0);
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU