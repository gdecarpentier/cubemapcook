// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "Texture2DArray.h"

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Context;

//////////////////////////////////////////////////////////////////////////////

class Texture2D : public Texture2DArray
{
public:
	void Initialize(int inWidth, int inHeight, int inLevels, DXGI_FORMAT inFormat, CPUAccess inCPUAccess);
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU