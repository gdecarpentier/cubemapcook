// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Texture2DArray.h"
#include "Device.h"
#include "Texture2DArray.h"
#include "Math/Algorithm.h"

using namespace Math;
using namespace Utility;

namespace GPU
{

//////////////////////////////////////////////////////////////////////////////

Texture2DArray::Texture2DArray() :
	mD3DTexture(nullptr)
{
}

//////////////////////////////////////////////////////////////////////////////

Texture2DArray::~Texture2DArray()
{
	if (mD3DTexture != nullptr)
		Finalize();
}


//////////////////////////////////////////////////////////////////////////////

Texture2DArray::Texture2DArray(Texture2DArray&& inOther) :
	Texture2DArray(static_cast<Texture2DArray&>(inOther))
{
	inOther.mD3DTexture = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

Texture2DArray& Texture2DArray::operator=(Texture2DArray&& inOther)
{
	ASSERT(mD3DTexture == nullptr); // Finalize this object before moving another one in.

	*this = static_cast<Texture2DArray&>(inOther);
	inOther.mD3DTexture = nullptr;
	return *this;
}

//////////////////////////////////////////////////////////////////////////////

void Texture2DArray::Initialize(int inWidth, int inHeight, int inLevels, int inSlices, DXGI_FORMAT inFormat, CPUAccess inCPUAccess)
{	
	Initialize(inWidth, inHeight, inLevels, inSlices, inFormat, inCPUAccess, 0);
}

//////////////////////////////////////////////////////////////////////////////

void Texture2DArray::Finalize()
{
	ASSERT(mD3DTexture != nullptr);
	mD3DTexture->Release();
	mD3DTexture = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

ID3D11Texture2D* Texture2DArray::Internal::GetD3DTexture(const Texture2DArray& inTexture2DArray)
{
	ASSERT(inTexture2DArray.mD3DTexture != nullptr);
	return inTexture2DArray.mD3DTexture;
}

//////////////////////////////////////////////////////////////////////////////

void Texture2DArray::Initialize(int inWidth, int inHeight, int inLevels, int inSlices, DXGI_FORMAT inFormat, CPUAccess inCPUAccess, UINT inMiscFlags)
{
	ASSERT(mD3DTexture == nullptr);
	ASSERT(inWidth >= 1 && inHeight >= 1 && inLevels >= 1 && inSlices >= 1);
	ASSERT(gMax(inWidth >> (inLevels - 1), inHeight >> (inLevels - 1)) > 0);

	mWidth = inWidth;
	mHeight = inHeight;
	mLevels = inLevels;
	mSlices = inSlices;
	mFormat = inFormat;
	mCPUAccess = inCPUAccess;
	
	D3D11_TEXTURE2D_DESC texture2d_desc;
	texture2d_desc.Width = mWidth;
	texture2d_desc.Height = mHeight;
	texture2d_desc.MipLevels = inLevels;
	texture2d_desc.ArraySize = inSlices;
	texture2d_desc.Format = inFormat;
	texture2d_desc.SampleDesc.Count = 1;
	texture2d_desc.SampleDesc.Quality = 0;
	texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
	texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	texture2d_desc.CPUAccessFlags = GPU::Internal::gGetD3DCPUAccessFlag(inCPUAccess);
	texture2d_desc.MiscFlags = inMiscFlags;

	if (inCPUAccess != CPUAccess::None)
	{
		texture2d_desc.Usage = D3D11_USAGE_STAGING;
		texture2d_desc.BindFlags = 0;
	}

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());

	// Create D3D texture
	HRESULT hr = d3d_device.CreateTexture2D(&texture2d_desc, 0, &mD3DTexture);
	gVerify(SUCCEEDED(hr), L"Failed to create Texture2DArray (%08X)\n", hr);
	ASSERT(mD3DTexture != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void gUploadTexture2DArrayRaw(const Texture2DArray& inTexture2DArray, const void* inRawBegin, int inRawPitch, int inLevel, int inSlice)
{
	ID3D11DeviceContext* d3dcontext = Context::Internal::GetD3DContext(gGetDevice().GetImmediateContext());
	ID3D11Texture2D* d3dtexture = Texture2DArray::Internal::GetD3DTexture(inTexture2DArray);

	ASSERT(inLevel >= 0 && inLevel < inTexture2DArray.GetLevels());
	ASSERT(inSlice >= 0 && inSlice < inTexture2DArray.GetSlices());
	ASSERT(inTexture2DArray.GetCPUAccess() == CPUAccess::Write || inTexture2DArray.GetCPUAccess() == CPUAccess::ReadWrite);
	int height = inTexture2DArray.GetHeight() >> inLevel;

	D3D11_MAPPED_SUBRESOURCE mapped;
	int subresource_index = inSlice * inTexture2DArray.GetLevels() + inLevel;
	HRESULT hr = d3dcontext->Map(d3dtexture, subresource_index, D3D11_MAP_WRITE, 0, &mapped);
	gVerify(SUCCEEDED(hr), L"Failed to map texture (%08X)", hr);
	ASSERT(mapped.pData != nullptr);

	const uint8_t* src = (const uint8_t*)inRawBegin;
	uint8_t* dst = reinterpret_cast<uint8_t*>(mapped.pData);
	int src_pitch = inRawPitch;
	int dst_pitch = mapped.RowPitch;
	ASSERT(dst_pitch >= src_pitch);
	for (int y = 0; y < height; ++y)
	{
		memcpy(dst, src, src_pitch);
		src += src_pitch;
		dst += dst_pitch;
	}
	d3dcontext->Unmap(d3dtexture, subresource_index);
}

//////////////////////////////////////////////////////////////////////////////

void gDownloadTexture2DArrayRaw(const Texture2DArray& inTexture2DArray, const void* inRawBegin, int inRawPitch, int inLevel, int inSlice)
{
	ID3D11DeviceContext* d3dcontext = Context::Internal::GetD3DContext(gGetDevice().GetImmediateContext());
	ID3D11Texture2D* d3dtexture = Texture2DArray::Internal::GetD3DTexture(inTexture2DArray);

	ASSERT(inLevel >= 0 && inLevel < inTexture2DArray.GetLevels());
	ASSERT(inSlice >= 0 && inSlice < inTexture2DArray.GetSlices());
	ASSERT(inTexture2DArray.GetCPUAccess() == CPUAccess::Read || inTexture2DArray.GetCPUAccess() == CPUAccess::ReadWrite);
	int height = inTexture2DArray.GetHeight() >> inLevel;

	D3D11_MAPPED_SUBRESOURCE mapped;
	int subresource_index = inSlice * inTexture2DArray.GetLevels() + inLevel;
	HRESULT hr = d3dcontext->Map(d3dtexture, subresource_index, D3D11_MAP_READ, 0, &mapped);
	gVerify(SUCCEEDED(hr), L"Failed to map texture (%08X)", hr);
	ASSERT(mapped.pData != nullptr);

	const uint8_t* src = reinterpret_cast<const uint8_t*>(mapped.pData);
	uint8_t* dst = (uint8_t*)inRawBegin;
	int src_pitch = mapped.RowPitch;
	int dst_pitch = inRawPitch;
	ASSERT(src_pitch >= dst_pitch);
	for (int y = 0; y < height; ++y)
	{
		memcpy(dst, src, dst_pitch);
		src += src_pitch;
		dst += dst_pitch;
	}
	d3dcontext->Unmap(d3dtexture, subresource_index);
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU