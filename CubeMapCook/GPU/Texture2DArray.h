// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include <vector>
#include "Constants.h"

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Context;

//////////////////////////////////////////////////////////////////////////////

class Texture2DArray
{
public:
	Texture2DArray();
	~Texture2DArray();

	Texture2DArray(Texture2DArray&& inOther);
	Texture2DArray& operator=(Texture2DArray&& inOther);

	void Initialize(int inWidth, int inHeight, int inLevels, int inSlices, DXGI_FORMAT inFormat, CPUAccess inCPUAccess);
	void Finalize();

	int GetWidth() const 
	{ 
		return mWidth;
	}

	int GetHeight() const 
	{ 
		return mHeight;
	}

	int GetLevels() const
	{ 
		return mLevels; 
	}

	int GetSlices() const
	{ 
		return mSlices; 
	}

	DXGI_FORMAT GetFormat() const
	{ 
		return mFormat; 
	}

	CPUAccess GetCPUAccess() const
	{ 
		return mCPUAccess;
	}

	struct Internal
	{
		static ID3D11Texture2D* GetD3DTexture(const Texture2DArray& inTexture2DArray);
	};

protected:
	void Initialize(int inWidth, int inHeight, int inLevels, int inSlices, DXGI_FORMAT inFormat, CPUAccess inCPUAccess, UINT inMiscFlags);

	Texture2DArray(const Texture2DArray&) = default;
	Texture2DArray& operator=(const Texture2DArray&) = default;

protected:
	ID3D11Texture2D* mD3DTexture;
	int mWidth;
	int mHeight;
	int mLevels;
	int mSlices;
	DXGI_FORMAT mFormat;
	CPUAccess mCPUAccess;
};

//////////////////////////////////////////////////////////////////////////////

void gUploadTexture2DArrayRaw(const Texture2DArray& inTexture2DArray, const void* inRawBegin, int inRawPitch, int inLevel, int inSlice);
void gDownloadTexture2DArrayRaw(const Texture2DArray& inTexture2DArray, const void* inRawBegin, int inRawPitch, int inLevel, int inSlice);

} // namespace GPU