// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "TextureCube.h"
#include "Device.h"
#include "Cube.h"

using namespace Math;
using namespace Utility;

namespace GPU
{

//////////////////////////////////////////////////////////////////////////////

void TextureCube::Initialize(int inResolution, int inLevels, DXGI_FORMAT inFormat, CPUAccess inCPUAccess)
{	
	Texture2DArray::Initialize(inResolution, inResolution, inLevels, 6, inFormat, inCPUAccess, D3D11_RESOURCE_MISC_TEXTURECUBE);
}

//////////////////////////////////////////////////////////////////////////////

void gUploadTextureCube(const Cube& inCube, const TextureCube& inTextureCube, int inFirstLevel, int inLastLevel)
{
	ASSERT(inLastLevel < inCube.mMips.size());

	for (int level = inFirstLevel; level <= inLastLevel; ++level)
	{
		int resolution = inCube.mResolution >> level;

		for (int face = 0; face < 6; ++face)
		{
			gUploadTexture2DArrayRaw(inTextureCube, inCube.mMips[level].mFaces[face], resolution * sizeof(Vec4), level, face);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

void gDownloadTextureCube(const TextureCube& inTextureCube, const Cube& inCube, int inFirstLevel, int inLastLevel)
{
	ASSERT(inLastLevel < inCube.mMips.size());

	for (int level = inFirstLevel; level <= inLastLevel; ++level)
	{
		int resolution = inCube.mResolution >> level;

		for (int face = 0; face < 6; ++face)
		{
			gDownloadTexture2DArrayRaw(inTextureCube, inCube.mMips[level].mFaces[face], resolution * sizeof(Vec4), level, face);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU