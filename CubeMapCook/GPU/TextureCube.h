// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "Texture2DArray.h"

struct Cube;

namespace GPU {

//////////////////////////////////////////////////////////////////////////////

class Context;

//////////////////////////////////////////////////////////////////////////////

class TextureCube : public Texture2DArray
{
public:
	void Initialize(int inResolution, int inLevels, DXGI_FORMAT inFormat, CPUAccess inCPUAccess);

	int GetResolution() const
	{
		return mWidth;
	}
};

//////////////////////////////////////////////////////////////////////////////

void gUploadTextureCube(const Cube& inCube, const TextureCube& inTextureCube, int inFirstLevel = 0, int inLastLevel = 0);
void gDownloadTextureCube(const TextureCube& inTextureCube, const Cube& inCube, int inFirstLevel = 0, int inLastLevel = 0);

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU