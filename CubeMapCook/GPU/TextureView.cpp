// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "TextureView.h"
#include "Texture2D.h"
#include "Texture2DArray.h"
#include "TextureCube.h"
#include "Device.h"

using namespace Math;
using namespace Utility;

namespace GPU
{

//////////////////////////////////////////////////////////////////////////////

TextureView::TextureView() :	
	mD3DSRV(nullptr)
{
}

//////////////////////////////////////////////////////////////////////////////

TextureView::~TextureView()
{
	if (mD3DSRV != nullptr)
		Finalize();
}

//////////////////////////////////////////////////////////////////////////////

void TextureView::Initialize(const Texture2D& inTexture2D, int inFirstLevel, int inLastLevel)
{
	ASSERT(mD3DSRV == nullptr);
	ASSERT(inFirstLevel <= inLastLevel);
	ASSERT(inLastLevel < inTexture2D.GetLevels());

	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	srv_desc.Format = (DXGI_FORMAT)0;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv_desc.Texture2D.MostDetailedMip = inFirstLevel;
	srv_desc.Texture2D.MipLevels = 1 + inLastLevel - inFirstLevel;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	ID3D11Texture2D* d3d_texture = Texture2DArray::Internal::GetD3DTexture(inTexture2D);
	HRESULT hr = d3d_device.CreateShaderResourceView(d3d_texture, &srv_desc, &mD3DSRV);
	gVerify(SUCCEEDED(hr), L"Failed to create Texture2D SRV (%08X)\n", hr);
	ASSERT(mD3DSRV != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void TextureView::Initialize(const Texture2DArray& inTexture2DArray, int inFirstLevel, int inLastLevel, int inFirstSlice, int inLastSlice)
{
	ASSERT(mD3DSRV == nullptr);
	ASSERT(inFirstLevel <= inLastLevel);
	ASSERT(inFirstSlice <= inLastSlice);
	ASSERT(inLastLevel < inTexture2DArray.GetLevels());
	ASSERT(inLastSlice < inTexture2DArray.GetSlices());

	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	srv_desc.Format = (DXGI_FORMAT)0;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srv_desc.Texture2DArray.MostDetailedMip = inFirstLevel;
	srv_desc.Texture2DArray.MipLevels = 1 + inLastLevel - inFirstLevel;
	srv_desc.Texture2DArray.FirstArraySlice = inFirstSlice;
	srv_desc.Texture2DArray.ArraySize = 1 + inLastSlice - inFirstSlice;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	ID3D11Texture2D* d3d_texture = Texture2DArray::Internal::GetD3DTexture(inTexture2DArray);
	HRESULT hr = d3d_device.CreateShaderResourceView(d3d_texture, &srv_desc, &mD3DSRV);
	gVerify(SUCCEEDED(hr), L"Failed to create Texture2DArray SRV (%08X)\n", hr);
	ASSERT(mD3DSRV != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void TextureView::Initialize(const TextureCube& inTextureCube, int inFirstLevel, int inLastLevel)
{
	ASSERT(mD3DSRV == nullptr);
	ASSERT(inFirstLevel <= inLastLevel);
	ASSERT(inLastLevel < inTextureCube.GetLevels());

	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	srv_desc.Format = (DXGI_FORMAT)0;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srv_desc.TextureCube.MostDetailedMip = inFirstLevel;
	srv_desc.TextureCube.MipLevels = 1 + inLastLevel - inFirstLevel;

	ID3D11Device& d3d_device = *Device::Internal::GetD3DDevice(gGetDevice());
	ID3D11Texture2D* d3d_texture = Texture2DArray::Internal::GetD3DTexture(inTextureCube);
	HRESULT hr = d3d_device.CreateShaderResourceView(d3d_texture, &srv_desc, &mD3DSRV);
	gVerify(SUCCEEDED(hr), L"Failed to create TextureCube SRV (%08X)\n", hr);
	ASSERT(mD3DSRV != nullptr);
}

//////////////////////////////////////////////////////////////////////////////

void TextureView::Finalize()
{
	ASSERT(mD3DSRV != nullptr);
	mD3DSRV->Release();
	mD3DSRV = nullptr;
}

//////////////////////////////////////////////////////////////////////////////

ID3D11ShaderResourceView* TextureView::Internal::GetD3DSRV(const TextureView& inTextureView)
{
	ASSERT(inTextureView.mD3DSRV != nullptr);
	return inTextureView.mD3DSRV;
}

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU