// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <d3d11.h>
#include "Constants.h"

namespace GPU {

class Texture2D;
class Texture2DArray;
class TextureCube;

//////////////////////////////////////////////////////////////////////////////

class TextureView
{
public:
	TextureView();
	~TextureView();

	void Initialize(const Texture2D& inTexture2D, int inFirstLevel, int inLastLevel);
	void Initialize(const Texture2DArray& inTexture2DArray, int inFirstLevel, int inLastLevel, int inFirstSlice, int inLastSlice);
	void Initialize(const TextureCube& inTextureCube, int inFirstLevel, int inLastLevel);
	void Finalize();

	struct Internal
	{
		static ID3D11ShaderResourceView* GetD3DSRV(const TextureView& inTextureView);
	};

private:
	ID3D11ShaderResourceView* mD3DSRV;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace GPU