// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Math {

//////////////////////////////////////////////////////////////////////////////

#define PI 3.141592653589793238f

//////////////////////////////////////////////////////////////////////////////

float gAbs(float inX);
float gCeil(float inX);
float gFloor(float inX);
float gRound(float inX);
int gRoundToInt(float inX);
float gCos(float inX);
float gSin(float inX);
float gSimpleSign(float inX);
float gSqr(float inX);
float gSqrt(float inX);
float gLog2(float inX);
float gPow(float inX, float inPower);

//////////////////////////////////////////////////////////////////////////////

template<typename TYPE>
TYPE gMax(TYPE inX, TYPE inY) { return inX > inY ? inX : inY; }

//////////////////////////////////////////////////////////////////////////////

template<typename TYPE>
TYPE gMin(TYPE inX, TYPE inY) { return inX > inY ? inY : inX; }

//////////////////////////////////////////////////////////////////////////////

} // namespace Math