// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "IVec.h"
#include "Algorithm.h"

namespace Math {

//////////////////////////////////////////////////////////////////////////////

IVec3::IVec3(const IVec2& inXY, int inZ) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZ)
{
}

//////////////////////////////////////////////////////////////////////////////

IVec4::IVec4(const IVec2& inXY, int inZ, int inW) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZ),
	mW(inW)
{
}

//////////////////////////////////////////////////////////////////////////////

IVec4::IVec4(const IVec2& inXY, const IVec2& inZW) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZW.GetX()),
	mW(inZW.GetY())
{
}

//////////////////////////////////////////////////////////////////////////////

IVec4::IVec4(const IVec3& inXYZ, int inW) :
	mX(inXYZ.GetX()),
	mY(inXYZ.GetY()),
	mZ(inXYZ.GetZ()),
	mW(inW)
{
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Math