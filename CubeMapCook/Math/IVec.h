// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Math {

//////////////////////////////////////////////////////////////////////////////

class IVec2;
class IVec3;
class IVec4;

//////////////////////////////////////////////////////////////////////////////

class IVec2
{
public:
	IVec2() { }
	explicit IVec2(int inValue) : mX(inValue), mY(inValue) { }
	IVec2(int inX, int inY) : mX(inX), mY(inY) { }

	IVec2 operator+(int inValue) const { return IVec2(mX + inValue, mY + inValue); }
	IVec2 operator+(const IVec2& inRHS) const { return IVec2(mX + inRHS.mX, mY + inRHS.mY); }
	IVec2& operator+=(int inValue) { return *this = *this + inValue; }
	IVec2& operator+=(const IVec2& inRHS) { return *this = *this + inRHS; }

	IVec2 operator-(int inValue) const { return IVec2(mX - inValue, mY - inValue); }
	IVec2 operator-(const IVec2& inRHS) const { return IVec2(mX - inRHS.mX, mY - inRHS.mY); }
	IVec2& operator-=(int inValue) { return *this = *this - inValue; }
	IVec2& operator-=(const IVec2& inRHS) { return *this = *this - inRHS; }

	IVec2 operator*(int inValue) const { return IVec2(mX * inValue, mY * inValue); }
	IVec2 operator*(const IVec2& inRHS) const { return IVec2(mX * inRHS.mX, mY * inRHS.mY); }
	IVec2& operator*=(int inValue) { return *this = *this * inValue; }
	IVec2& operator*=(const IVec2& inRHS) { return *this = *this * inRHS; }

	IVec2 operator/(int inValue) const { return IVec2(mX / inValue, mY / inValue); }
	IVec2 operator/(const IVec2& inRHS) const { return IVec2(mX / inRHS.mX, mY / inRHS.mY); }
	IVec2& operator/=(int inValue) { return *this = *this / inValue; }
	IVec2& operator/=(const IVec2& inRHS) { return *this = *this / inRHS; }

	int GetX() const { return mX; }
	int GetY() const { return mY; }
	void SetX(int inValue) { mX = inValue; }
	void SetY(int inValue) { mY = inValue; }

private:
	int mX, mY;
};

//////////////////////////////////////////////////////////////////////////////

class IVec3
{
public:
	IVec3() { }
	explicit IVec3(int inValue) : mX(inValue), mY(inValue), mZ(inValue) { }
	explicit IVec3(const IVec4& inXYZ);
	IVec3(const IVec2& inXY, int mZ);
	IVec3(int inX, int inY, int inZ) : mX(inX), mY(inY), mZ(inZ) { }

	IVec3 operator+(int inValue) const { return IVec3(mX + inValue, mY + inValue, mZ + inValue); }
	IVec3 operator+(const IVec3& inRHS) const { return IVec3(mX + inRHS.mX, mY + inRHS.mY, mZ + inRHS.mZ); }
	IVec3& operator+=(int inValue) { return *this = *this + inValue; }
	IVec3& operator+=(const IVec3& inRHS) { return *this = *this + inRHS; }

	IVec3 operator-(int inValue) const { return IVec3(mX - inValue, mY - inValue, mZ - inValue); }
	IVec3 operator-(const IVec3& inRHS) const { return IVec3(mX - inRHS.mX, mY - inRHS.mY, mZ - inRHS.mZ); }
	IVec3& operator-=(int inValue) { return *this = *this - inValue; }
	IVec3& operator-=(const IVec3& inRHS) { return *this = *this - inRHS; }

	IVec3 operator*(int inValue) const { return IVec3(mX * inValue, mY * inValue, mZ * inValue); }
	IVec3 operator*(const IVec3& inRHS) const { return IVec3(mX * inRHS.mX, mY * inRHS.mY, mZ * inRHS.mZ); }
	IVec3& operator*=(int inValue) { return *this = *this * inValue; }
	IVec3& operator*=(const IVec3& inRHS) { return *this = *this * inRHS; }

	IVec3 operator/(int inValue) const { return IVec3(mX / inValue, mY / inValue, mZ / inValue); }
	IVec3 operator/(const IVec3& inRHS) const { return IVec3(mX / inRHS.mX, mY / inRHS.mY, mZ / inRHS.mZ); }
	IVec3& operator/=(int inValue) { return *this = *this / inValue; }
	IVec3& operator/=(const IVec3& inRHS) { return *this = *this / inRHS; }

	int GetX() const { return mX; }
	int GetY() const { return mY; }
	int GetZ() const { return mZ; }
	IVec2 GetXY() const { return IVec2(mX, mY); }
	IVec2 GetXZ() const { return IVec2(mX, mZ); }
	IVec2 GetYZ() const { return IVec2(mY, mZ); }

	void SetX(int inValue) { mX = inValue; }
	void SetY(int inValue) { mY = inValue; }
	void SetZ(int inValue) { mZ = inValue; }
	void SetXY(const IVec2& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); }
	void SetXZ(const IVec2& inValue) { mX = inValue.GetX(); mZ = inValue.GetY(); }
	void SetYZ(const IVec2& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); }

private:
	int mX, mY, mZ;
};

//////////////////////////////////////////////////////////////////////////////

class IVec4
{
public:
	IVec4() { }
	explicit IVec4(int inValue) : mX(inValue), mY(inValue), mZ(inValue), mW(inValue) { }
	IVec4(const IVec2& inXY, const IVec2& inZW);
	IVec4(const IVec2& inXY, int mZ, int mW);
	IVec4(const IVec3& inXYZ, int mW);
	IVec4(int inX, int inY, int inZ, int inW) : mX(inX), mY(inY), mZ(inZ), mW(inW) { }

	IVec4 operator+(int inValue) const { return IVec4(mX + inValue, mY + inValue, mZ + inValue, mW + inValue); }
	IVec4 operator+(const IVec4& inRHS) const { return IVec4(mX + inRHS.mX, mY + inRHS.mY, mZ + inRHS.mZ, mW + inRHS.mW); }
	IVec4& operator+=(int inValue) { return *this = *this + inValue; }
	IVec4& operator+=(const IVec4& inRHS) { return *this = *this + inRHS; }

	IVec4 operator-(int inValue) const { return IVec4(mX - inValue, mY - inValue, mZ - inValue, mW - inValue); }
	IVec4 operator-(const IVec4& inRHS) const { return IVec4(mX - inRHS.mX, mY - inRHS.mY, mZ - inRHS.mZ, mW - inRHS.mW); }
	IVec4& operator-=(int inValue) { return *this = *this - inValue; }
	IVec4& operator-=(const IVec4& inRHS) { return *this = *this - inRHS; }
	
	IVec4 operator*(int inValue) const { return IVec4(mX * inValue, mY * inValue, mZ * inValue, mW * inValue); }
	IVec4 operator*(const IVec4& inRHS) const { return IVec4(mX * inRHS.mX, mY * inRHS.mY, mZ * inRHS.mZ, mW * inRHS.mW); }
	IVec4& operator*=(int inValue) { return *this = *this * inValue; }
	IVec4& operator*=(const IVec4& inRHS) { return *this = *this * inRHS; }
	
	IVec4 operator/(int inValue) const { return IVec4(mX / inValue, mY / inValue, mZ / inValue, mW / inValue); }
	IVec4 operator/(const IVec4& inRHS) const { return IVec4(mX / inRHS.mX, mY / inRHS.mY, mZ / inRHS.mZ, mW / inRHS.mW); }
	IVec4& operator/=(int inValue) { return *this = *this / inValue; }
	IVec4& operator/=(const IVec4& inRHS) { return *this = *this / inRHS; }

	int GetX() const { return mX; }
	int GetY() const { return mY; }
	int GetZ() const { return mZ; }
	int GetW() const { return mW; }
	IVec2 GetXY() const { return IVec2(mX, mY); }
	IVec2 GetXZ() const { return IVec2(mX, mZ); }
	IVec2 GetXW() const { return IVec2(mX, mW); }
	IVec2 GetYZ() const { return IVec2(mY, mZ); }
	IVec2 GetYW() const { return IVec2(mY, mW); }
	IVec2 GetZW() const { return IVec2(mZ, mW); }
	IVec3 GetXYZ() const { return IVec3(mX, mY, mZ); }
	IVec3 GetXYW() const { return IVec3(mX, mY, mW); }
	IVec3 GetYZW() const { return IVec3(mY, mZ, mW); }

	void SetX(int inValue) { mX = inValue; }
	void SetY(int inValue) { mY = inValue; }
	void SetZ(int inValue) { mZ = inValue; }
	void SetW(int inValue) { mW = inValue; }
	void SetXY(const IVec2& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); }
	void SetXZ(const IVec2& inValue) { mX = inValue.GetX(); mZ = inValue.GetY(); }
	void SetXW(const IVec2& inValue) { mX = inValue.GetX(); mW = inValue.GetY(); }
	void SetYZ(const IVec2& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); }
	void SetYW(const IVec2& inValue) { mY = inValue.GetX(); mW = inValue.GetY(); }
	void SetZW(const IVec2& inValue) { mZ = inValue.GetX(); mW = inValue.GetY(); }
	void SetXYZ(const IVec3& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); mZ = inValue.GetZ(); }
	void SetXYW(const IVec3& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); mW = inValue.GetZ(); }
	void SetYZW(const IVec3& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); mW = inValue.GetZ(); }

private:
	int mX, mY, mZ, mW;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace Math