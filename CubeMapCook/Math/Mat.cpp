// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Vec.h"
#include "Mat.h"
#include "Algorithm.h"

namespace Math {

//////////////////////////////////////////////////////////////////////////////

Mat2x2::Mat2x2(const Vec2& inX, const Vec2& inY) :
	mXX(inX.GetX()),
	mXY(inX.GetY()),
	mYX(inY.GetX()),
	mYY(inY.GetY())
{
}

//////////////////////////////////////////////////////////////////////////////

Vec2 Mat2x2::operator*(const Vec2& inValue) const 
{ 
	return Vec2(gDot(GetX(), inValue), gDot(GetY(), inValue));
}

//////////////////////////////////////////////////////////////////////////////

Mat2x2 Mat2x2::operator*(const Mat2x2& inRHS) const 
{ 
	return Mat2x2(gDot(GetX(), inRHS.GetColumnX()), gDot(GetX(), inRHS.GetColumnY()), gDot(GetY(), inRHS.GetColumnX()), gDot(GetY(), inRHS.GetColumnY()));
}

//////////////////////////////////////////////////////////////////////////////

Vec2 Mat2x2::GetX() const 
{ 
	return Vec2(mXX, mXY); 
}

Vec2 Mat2x2::GetY() const
{
	return Vec2(mYX, mYY); 
}

//////////////////////////////////////////////////////////////////////////////

Vec2 Mat2x2::GetColumnX() const
{
	return Vec2(mXX, mYX);
}

Vec2 Mat2x2::GetColumnY() const
{
	return Vec2(mXY, mYY);
}

//////////////////////////////////////////////////////////////////////////////

void Mat2x2::SetX(const Vec2& inValue) 
{ 
	mXX = inValue.GetX(); 
	mXY = inValue.GetY(); 
}

void Mat2x2::SetY(const Vec2& inValue) 
{ 
	mYX = inValue.GetX(); 
	mYY = inValue.GetY();
}

//////////////////////////////////////////////////////////////////////////////

float gDeterminant(const Mat2x2& inValue)
{
	return inValue.GetXX() * inValue.GetYY() - inValue.GetXY() * inValue.GetYX();
}

Mat2x2 gTransposed(const Mat2x2& inValue)
{
	return Mat2x2(inValue.GetXX(), inValue.GetYX(), inValue.GetXY(), inValue.GetYY());
}

Mat2x2 gAdjugate(const Mat2x2& inValue)
{
	return Mat2x2(inValue.GetYY(), -inValue.GetXY(), -inValue.GetYX(), inValue.GetXX());
}

Mat2x2 gInversed(const Mat2x2& inValue)
{
	return gAdjugate(inValue) / gDeterminant(inValue);
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Math