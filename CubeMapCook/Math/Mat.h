// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Math {

class Vec2;

//////////////////////////////////////////////////////////////////////////////

class Mat2x2
{
public:
	Mat2x2() { }
	explicit Mat2x2(float inValue) : mXX(inValue), mXY(inValue), mYX(inValue), mYY(inValue) { }
	Mat2x2(const Vec2& inXY, const Vec2& inZW);
	Mat2x2(float inXX, float inXY, float inYX, float inYY) : mXX(inXX), mXY(inXY), mYX(inYX), mYY(inYY) { }

	bool operator==(const Mat2x2& inRHS) const { return mXX == inRHS.mXX && mXY == inRHS.mXY && mYX == inRHS.mYX && mYY == inRHS.mYY; }
	bool operator!=(const Mat2x2& inRHS) const { return !(*this == inRHS); }

	Mat2x2 operator+(const Mat2x2& inRHS) const { return Mat2x2(mXX + inRHS.mXX, mXY + inRHS.mXY, mYX + inRHS.mYX, mYY + inRHS.mYY); }
	Mat2x2& operator+=(const Mat2x2& inRHS) { return *this = *this + inRHS; }

	Mat2x2 operator-(const Mat2x2& inRHS) const { return Mat2x2(mXX - inRHS.mXX, mXY - inRHS.mXY, mYX - inRHS.mYX, mYY - inRHS.mYY); }
	Mat2x2& operator-=(const Mat2x2& inRHS) { return *this = *this - inRHS; }

	Mat2x2 operator*(float inValue) const { return Mat2x2(mXX * inValue, mXY * inValue, mYX * inValue, mYY * inValue); }
	Vec2 operator*(const Vec2& inValue) const;
	Mat2x2 operator*(const Mat2x2& inRHS) const;
	Mat2x2& operator*=(const Mat2x2& inRHS) { return *this = *this * inRHS; }

	Mat2x2 operator/(float inValue) const { return Mat2x2(mXX / inValue, mXY / inValue, mYX / inValue, mYY / inValue); }
	
	float GetXX() const { return mXX; }
	float GetXY() const { return mXY; }
	float GetYX() const { return mYX; }
	float GetYY() const { return mYY; }
	Vec2 GetX() const;
	Vec2 GetY() const;

	Vec2 GetColumnX() const;
	Vec2 GetColumnY() const;

	void SetXX(float inValue) { mXX = inValue; }
	void SetXY(float inValue) { mXY = inValue; }
	void SetYX(float inValue) { mYX = inValue; }
	void SetYY(float inValue) { mYY = inValue; }
	void SetX(const Vec2& inValue);
	void SetY(const Vec2& inValue);

	static Mat2x2 Identity() { return Mat2x2(1.0f, 0.0f, 0.0f, 1.0f); }

private:
	float mXX, mXY, mYX, mYY;
};

//////////////////////////////////////////////////////////////////////////////

float gDeterminant(const Mat2x2& inValue);
Mat2x2 gTransposed(const Mat2x2& inValue);
Mat2x2 gAdjugate(const Mat2x2& inValue);
Mat2x2 gInversed(const Mat2x2& inValue);

//////////////////////////////////////////////////////////////////////////////


} // namespace Math