// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "SH1.h"
#include "Vec.h"
#include "Algorithm.h"

namespace Math {

//////////////////////////////////////////////////////////////////////////////



void gAccumulateWeightedSH1(WeightedSH1& inWeightedSH1, const Vec3& inNormal, const Vec4& gAccumulateWeightedSH1)
{
	Vec3 weighted_value = gAccumulateWeightedSH1.GetXYZ();
	float weight = gAccumulateWeightedSH1.GetW();

	inWeightedSH1.sh1.c[0] += weighted_value;
	inWeightedSH1.sh1.c[1] += weighted_value * inNormal.GetX();
	inWeightedSH1.sh1.c[2] += weighted_value * inNormal.GetY();
	inWeightedSH1.sh1.c[3] += weighted_value * inNormal.GetZ();
	inWeightedSH1.w += weight;
}

//////////////////////////////////////////////////////////////////////////////

SH1 gGetNormalizedWeightedSH1(WeightedSH1& inWeightedSH1)
{
	// For a properly scaled SH1, w should be 4*PI, and c[0] *= w0 and C[1..3] *= w2 where w0 = (1/(2*sqrt(PI)))^2 = 1/(4*PI)
	// and w1 = (sqrt(3)/(2*sqrt(PI)))^2 = 3/(4*PI), but because we divide by w, the multiplication factors to 1.0 and 3.0, respectively.

	float scale = 1.0f / inWeightedSH1.w;
	SH1 sh1;
	sh1.c[0] = inWeightedSH1.sh1.c[0] * scale;
	sh1.c[1] = inWeightedSH1.sh1.c[1] * 3.0f * scale;
	sh1.c[2] = inWeightedSH1.sh1.c[2] * 3.0f * scale;
	sh1.c[3] = inWeightedSH1.sh1.c[3] * 3.0f * scale;

	return sh1;
}

//////////////////////////////////////////////////////////////////////////////

void gScaleSH1(SH1& inSH1, float inScale)
{
	for (int i = 0; i < 4; ++i)
	{
		inSH1.c[i] *= inScale;
	}
}

//////////////////////////////////////////////////////////////////////////////

SH1 gGetSH1ConvolvedWithCosineLobe(SH1& inNormalizedSH1, float inClampThreshold)
{
	// Mathematica code showing that the weight for the zeroth band (sh0) and first band (sh1) are 1 and 2/3, respectively.
	// (* Note that 2 Pi Integrate[Sin[a] * f[a], { a, 0, Pi / 2 }] is the unbiased integral of f over the hemisphere) *)
	// (* See "Stupid Spherical Harmonics (SH) Tricks, Peter-Pike Sloan for more details about the SH1 basis and its convolution. *)
	// cosinelobe = Cos[a] / Pi;
	// shweight0 = 1 / (2 Sqrt[Pi]); (* standard normalization constant for the zeroth band basis *)
	// shweight1 = Sqrt[3] / (2 Sqrt[Pi]); (* standard normalization constant for the first band basis *)
	// shconvolutionweight0 = Sqrt[4 Pi / (2 * 0 + 1)]; (* standard normalization constant for the zeroth band for convolution *)
	// shconvolutionweight1 = Sqrt[4 Pi / (2 * 1 + 1)]; (* standard normalization constant for the first band for convolution *)
	// w0 = 2 Pi Integrate[Sin[a] * cosinelobe * shweight0, { a, 0, Pi / 2 }] * shconvolutionweight0 
	// w1 = 2 Pi Integrate[Sin[a] * cosinelobe * Cos[a] * shweight1, { a, 0, Pi / 2 }] * shconvolutionweight1 

	const float w0 = 1.0f;			// missing the 1/(4*PI) because this is assumed to already be normalized out
	const float w1 = 2.0f / 3.0f;	// missing the 1/(4*PI) because this is assumed to already be normalized out

	SH1 result;
	result.c[0] = inNormalizedSH1.c[0] * w0;
	result.c[1] = inNormalizedSH1.c[1] * w1;
	result.c[2] = inNormalizedSH1.c[2] * w1;
	result.c[3] = inNormalizedSH1.c[3] * w1;

	Vec3& v0 = result.c[0];
	if (v0.GetX() < 0.0f) v0.SetX(0.0f);
	if (v0.GetY() < 0.0f) v0.SetY(0.0f);
	if (v0.GetZ() < 0.0f) v0.SetZ(0.0f);

	Vec3 scaled_v0 = v0 / (1.0f + inClampThreshold);
	for (int i = 1; i < 4; ++i)
	{
		Vec3& v = result.c[i];
		if (gAbs(v.GetX()) > scaled_v0.GetX()) v.SetX(gSimpleSign(v.GetX()) * scaled_v0.GetX());
		if (gAbs(v.GetY()) > scaled_v0.GetY()) v.SetY(gSimpleSign(v.GetY()) * scaled_v0.GetY());
		if (gAbs(v.GetZ()) > scaled_v0.GetZ()) v.SetZ(gSimpleSign(v.GetZ()) * scaled_v0.GetZ());
	}

	return result;
}

//////////////////////////////////////////////////////////////////////////////

Vec3 gEvaluateSH1(const SH1& inSH1, const Vec3& inNormal)
{
	return inSH1.c[0] + inSH1.c[1] * inNormal.GetX() + inSH1.c[2] * inNormal.GetY() + inSH1.c[3] * inNormal.GetZ();
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Math