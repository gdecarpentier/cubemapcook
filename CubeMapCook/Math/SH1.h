// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include "Vec.h"

namespace Math {

//////////////////////////////////////////////////////////////////////////////

struct SH1
{
	Math::Vec3 c[4];

	SH1() { }

	explicit SH1(float inValue)
	{
		for (int i = 0; i < 4; ++i)
			c[i] = Math::Vec3(inValue, inValue, inValue);
	}
};

struct WeightedSH1
{
	SH1 sh1;
	float w;

	WeightedSH1() { };

	explicit WeightedSH1(float inValue) :
		sh1(inValue),
		w(inValue)
	{
	}
};

//////////////////////////////////////////////////////////////////////////////

void gAccumulateWeightedSH1(WeightedSH1& inWeightedSH1, const Math::Vec3& inNormal, const Math::Vec4& inWeightedValueAndWeight);

SH1 gGetNormalizedWeightedSH1(WeightedSH1& inSH1);

void gScaleSH1(SH1& inSH1, float inScale);

SH1 gGetSH1ConvolvedWithCosineLobe(SH1& inNormalizedSH1, float inClampThreshold);

Math::Vec3 gEvaluateSH1(const SH1& inSH1, const Math::Vec3& inNormal);

} // namespace Math