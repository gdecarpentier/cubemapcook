// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Vec.h"
#include "Algorithm.h"

namespace Math {

//////////////////////////////////////////////////////////////////////////////

Vec3::Vec3(const Vec2& inXY, float inZ) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZ)
{
}

//////////////////////////////////////////////////////////////////////////////

Vec4::Vec4(const Vec2& inXY, float inZ, float inW) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZ),
	mW(inW)
{
}

//////////////////////////////////////////////////////////////////////////////

Vec4::Vec4(const Vec2& inXY, const Vec2& inZW) :
	mX(inXY.GetX()),
	mY(inXY.GetY()),
	mZ(inZW.GetX()),
	mW(inZW.GetY())
{
}

//////////////////////////////////////////////////////////////////////////////

Vec4::Vec4(const Vec3& inXYZ, float inW) :
	mX(inXYZ.GetX()),
	mY(inXYZ.GetY()),
	mZ(inXYZ.GetZ()),
	mW(inW)
{
}

//////////////////////////////////////////////////////////////////////////////

float gDot(const Vec2& inLHS, const Vec2& inRHS) 
{ 
	return inLHS.GetX() * inRHS.GetX() + inLHS.GetY() * inRHS.GetY(); 
}

float gDot(const Vec3& inLHS, const Vec3& inRHS) 
{ 
	return inLHS.GetX() * inRHS.GetX() + inLHS.GetY() * inRHS.GetY() + inLHS.GetZ() * inRHS.GetZ(); 
}

float gDot(const Vec4& inLHS, const Vec4& inRHS) 
{ 
	return inLHS.GetX() * inRHS.GetX() + inLHS.GetY() * inRHS.GetY() + inLHS.GetZ() * inRHS.GetZ() + inLHS.GetW() * inRHS.GetW(); 
}

//////////////////////////////////////////////////////////////////////////////

float gLength(const Vec2& inVec) 
{
	return gSqrt(gDot(inVec, inVec)); 
}

float gLength(const Vec3& inVec) 
{
	return gSqrt(gDot(inVec, inVec)); 
}

float gLength(const Vec4& inVec) 
{ 
	return gSqrt(gDot(inVec, inVec)); 
}

//////////////////////////////////////////////////////////////////////////////

Vec2 gNormalized(const Vec2& inVec)
{
	return inVec * (1.0f / gLength(inVec)); 
}

Vec3 gNormalized(const Vec3& inVec)
{
	return inVec * (1.0f / gLength(inVec)); 
}

Vec4 gNormalized(const Vec4& inVec) 
{ 
	return inVec * (1.0f / gLength(inVec)); 
}

//////////////////////////////////////////////////////////////////////////////

Vec3 gCross(const Vec3& inLHS, const Vec3& inRHS) 
{
	return Vec3(inLHS.GetY() * inRHS.GetZ() - inLHS.GetZ() * inRHS.GetY(), inLHS.GetZ() * inRHS.GetX() - inLHS.GetX() * inRHS.GetZ(), inLHS.GetX() * inRHS.GetY() - inLHS.GetY() * inRHS.GetX()); 
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Math