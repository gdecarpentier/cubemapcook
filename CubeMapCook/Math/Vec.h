// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Math {

class Vec2;
class Vec3;
class Vec4;

//////////////////////////////////////////////////////////////////////////////

class Vec2
{
public:
	Vec2() { }
	explicit Vec2(float inValue) : mX(inValue), mY(inValue) { }
	Vec2(float inX, float inY) : mX(inX), mY(inY) { }

	bool operator==(const Vec2& inRHS) const { return mX == inRHS.mX && mY == inRHS.mY; }
	bool operator!=(const Vec2& inRHS) const { return !(*this == inRHS); }

	Vec2 operator+(float inValue) const { return Vec2(mX + inValue, mY + inValue); }
	Vec2 operator+(const Vec2& inRHS) const { return Vec2(mX + inRHS.mX, mY + inRHS.mY); }
	Vec2& operator+=(float inValue) { return *this = *this + inValue; }
	Vec2& operator+=(const Vec2& inRHS) { return *this = *this + inRHS; }

	Vec2 operator-(float inValue) const { return Vec2(mX - inValue, mY - inValue); }
	Vec2 operator-(const Vec2& inRHS) const { return Vec2(mX - inRHS.mX, mY - inRHS.mY); }
	Vec2& operator-=(float inValue) { return *this = *this - inValue; }
	Vec2& operator-=(const Vec2& inRHS) { return *this = *this - inRHS; }

	Vec2 operator*(float inValue) const { return Vec2(mX * inValue, mY * inValue); }
	Vec2 operator*(const Vec2& inRHS) const { return Vec2(mX * inRHS.mX, mY * inRHS.mY); }
	Vec2& operator*=(float inValue) { return *this = *this * inValue; }
	Vec2& operator*=(const Vec2& inRHS) { return *this = *this * inRHS; }

	Vec2 operator/(float inValue) const { return Vec2(mX / inValue, mY / inValue); }
	Vec2 operator/(const Vec2& inRHS) const { return Vec2(mX / inRHS.mX, mY / inRHS.mY); }
	Vec2& operator/=(float inValue) { return *this = *this / inValue; }
	Vec2& operator/=(const Vec2& inRHS) { return *this = *this / inRHS; }

	float GetX() const { return mX; }
	float GetY() const { return mY; }
	void SetX(float inValue) { mX = inValue; }
	void SetY(float inValue) { mY = inValue; }

private:
	float mX, mY;
};

//////////////////////////////////////////////////////////////////////////////

class Vec3
{
public:
	Vec3() { }
	explicit Vec3(float inValue) : mX(inValue), mY(inValue), mZ(inValue) { }
	explicit Vec3(const Vec4& inXYZ);
	Vec3(const Vec2& inXY, float mZ);
	Vec3(float inX, float inY, float inZ) : mX(inX), mY(inY), mZ(inZ) { }

	bool operator==(const Vec3& inRHS) const { return mX == inRHS.mX && mY == inRHS.mY && mZ == inRHS.mZ; }
	bool operator!=(const Vec3& inRHS) const { return !(*this == inRHS); }

	Vec3 operator+(float inValue) const { return Vec3(mX + inValue, mY + inValue, mZ + inValue); }
	Vec3 operator+(const Vec3& inRHS) const { return Vec3(mX + inRHS.mX, mY + inRHS.mY, mZ + inRHS.mZ); }
	Vec3& operator+=(float inValue) { return *this = *this + inValue; }
	Vec3& operator+=(const Vec3& inRHS) { return *this = *this + inRHS; }

	Vec3 operator-(float inValue) const { return Vec3(mX - inValue, mY - inValue, mZ - inValue); }
	Vec3 operator-(const Vec3& inRHS) const { return Vec3(mX - inRHS.mX, mY - inRHS.mY, mZ - inRHS.mZ); }
	Vec3& operator-=(float inValue) { return *this = *this - inValue; }
	Vec3& operator-=(const Vec3& inRHS) { return *this = *this - inRHS; }

	Vec3 operator*(float inValue) const { return Vec3(mX * inValue, mY * inValue, mZ * inValue); }
	Vec3 operator*(const Vec3& inRHS) const { return Vec3(mX * inRHS.mX, mY * inRHS.mY, mZ * inRHS.mZ); }
	Vec3& operator*=(float inValue) { return *this = *this * inValue; }
	Vec3& operator*=(const Vec3& inRHS) { return *this = *this * inRHS; }

	Vec3 operator/(float inValue) const { return Vec3(mX / inValue, mY / inValue, mZ / inValue); }
	Vec3 operator/(const Vec3& inRHS) const { return Vec3(mX / inRHS.mX, mY / inRHS.mY, mZ / inRHS.mZ); }
	Vec3& operator/=(float inValue) { return *this = *this / inValue; }
	Vec3& operator/=(const Vec3& inRHS) { return *this = *this / inRHS; }

	float GetX() const { return mX; }
	float GetY() const { return mY; }
	float GetZ() const { return mZ; }
	Vec2 GetXY() const { return Vec2(mX, mY); }
	Vec2 GetXZ() const { return Vec2(mX, mZ); }
	Vec2 GetYZ() const { return Vec2(mY, mZ); }

	void SetX(float inValue) { mX = inValue; }
	void SetY(float inValue) { mY = inValue; }
	void SetZ(float inValue) { mZ = inValue; }
	void SetXY(const Vec2& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); }
	void SetXZ(const Vec2& inValue) { mX = inValue.GetX(); mZ = inValue.GetY(); }
	void SetYZ(const Vec2& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); }

private:
	float mX, mY, mZ;
};

//////////////////////////////////////////////////////////////////////////////

class Vec4
{
public:
	Vec4() { }
	explicit Vec4(float inValue) : mX(inValue), mY(inValue), mZ(inValue), mW(inValue) { }
	Vec4(const Vec2& inXY, const Vec2& inZW);
	Vec4(const Vec2& inXY, float mZ, float mW);
	Vec4(const Vec3& inXYZ, float mW);
	Vec4(float inX, float inY, float inZ, float inW) : mX(inX), mY(inY), mZ(inZ), mW(inW) { }

	bool operator==(const Vec4& inRHS) const { return mX == inRHS.mX && mY == inRHS.mY && mZ == inRHS.mZ && mW == inRHS.mW; }
	bool operator!=(const Vec4& inRHS) const { return !(*this == inRHS); }

	Vec4 operator+(float inValue) const { return Vec4(mX + inValue, mY + inValue, mZ + inValue, mW + inValue); }
	Vec4 operator+(const Vec4& inRHS) const { return Vec4(mX + inRHS.mX, mY + inRHS.mY, mZ + inRHS.mZ, mW + inRHS.mW); }
	Vec4& operator+=(float inValue) { return *this = *this + inValue; }
	Vec4& operator+=(const Vec4& inRHS) { return *this = *this + inRHS; }

	Vec4 operator-(float inValue) const { return Vec4(mX - inValue, mY - inValue, mZ - inValue, mW - inValue); }
	Vec4 operator-(const Vec4& inRHS) const { return Vec4(mX - inRHS.mX, mY - inRHS.mY, mZ - inRHS.mZ, mW - inRHS.mW); }
	Vec4& operator-=(float inValue) { return *this = *this - inValue; }
	Vec4& operator-=(const Vec4& inRHS) { return *this = *this - inRHS; }
	
	Vec4 operator*(float inValue) const { return Vec4(mX * inValue, mY * inValue, mZ * inValue, mW * inValue); }
	Vec4 operator*(const Vec4& inRHS) const { return Vec4(mX * inRHS.mX, mY * inRHS.mY, mZ * inRHS.mZ, mW * inRHS.mW); }
	Vec4& operator*=(float inValue) { return *this = *this * inValue; }
	Vec4& operator*=(const Vec4& inRHS) { return *this = *this * inRHS; }
	
	Vec4 operator/(float inValue) const { return Vec4(mX / inValue, mY / inValue, mZ / inValue, mW / inValue); }
	Vec4 operator/(const Vec4& inRHS) const { return Vec4(mX / inRHS.mX, mY / inRHS.mY, mZ / inRHS.mZ, mW / inRHS.mW); }
	Vec4& operator/=(float inValue) { return *this = *this / inValue; }
	Vec4& operator/=(const Vec4& inRHS) { return *this = *this / inRHS; }

	float GetX() const { return mX; }
	float GetY() const { return mY; }
	float GetZ() const { return mZ; }
	float GetW() const { return mW; }
	Vec2 GetXY() const { return Vec2(mX, mY); }
	Vec2 GetXZ() const { return Vec2(mX, mZ); }
	Vec2 GetXW() const { return Vec2(mX, mW); }
	Vec2 GetYZ() const { return Vec2(mY, mZ); }
	Vec2 GetYW() const { return Vec2(mY, mW); }
	Vec2 GetZW() const { return Vec2(mZ, mW); }
	Vec3 GetXYZ() const { return Vec3(mX, mY, mZ); }
	Vec3 GetXYW() const { return Vec3(mX, mY, mW); }
	Vec3 GetYZW() const { return Vec3(mY, mZ, mW); }

	void SetX(float inValue) { mX = inValue; }
	void SetY(float inValue) { mY = inValue; }
	void SetZ(float inValue) { mZ = inValue; }
	void SetW(float inValue) { mW = inValue; }
	void SetXY(const Vec2& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); }
	void SetXZ(const Vec2& inValue) { mX = inValue.GetX(); mZ = inValue.GetY(); }
	void SetXW(const Vec2& inValue) { mX = inValue.GetX(); mW = inValue.GetY(); }
	void SetYZ(const Vec2& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); }
	void SetYW(const Vec2& inValue) { mY = inValue.GetX(); mW = inValue.GetY(); }
	void SetZW(const Vec2& inValue) { mZ = inValue.GetX(); mW = inValue.GetY(); }
	void SetXYZ(const Vec3& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); mZ = inValue.GetZ(); }
	void SetXYW(const Vec3& inValue) { mX = inValue.GetX(); mY = inValue.GetY(); mW = inValue.GetZ(); }
	void SetYZW(const Vec3& inValue) { mY = inValue.GetX(); mZ = inValue.GetY(); mW = inValue.GetZ(); }

private:
	float mX, mY, mZ, mW;
};

//////////////////////////////////////////////////////////////////////////////

float gDot(const Vec2& inLHS, const Vec2& inRHS);
float gDot(const Vec3& inLHS, const Vec3& inRHS);
float gDot(const Vec4& inLHS, const Vec4& inRHS);

float gLength(const Vec2& inVec);
float gLength(const Vec3& inVec);
float gLength(const Vec4& inVec);

Vec2 gNormalized(const Vec2& inVec);
Vec3 gNormalized(const Vec3& inVec);
Vec4 gNormalized(const Vec4& inVec);

Vec3 gCross(const Vec3& inLHS, const Vec3& inRHS);

//////////////////////////////////////////////////////////////////////////////

} // namespace Math