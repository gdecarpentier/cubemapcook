// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include <d3d11.h>
#include <stdint.h>
#include "Utility/TypeTraits.h"
#include "Utility/Verify.h"
