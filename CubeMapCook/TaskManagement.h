// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

void gInitISPCTaskManagement(int inRelativeThreadPriority);
