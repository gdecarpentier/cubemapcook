// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "ColorConvert.h"
#include "../Math/Vec.h"
#include "../Math/Algorithm.h"

using namespace Math;

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters::ColorConvertParameters(float inLinearScale, float inLinearThreshold, float inGammaPreBias, float inGammaPower, float inGammaPostScale, float inGammaPostBias) :
	mLinearScale(inLinearScale),
	mLinearThreshold(inLinearThreshold),
	mGammaPreBias(inGammaPreBias),
	mGammaPower(inGammaPower),
	mGammaPostScale(inGammaPostScale),
	mGammaPostBias(inGammaPostBias)
{
}

//////////////////////////////////////////////////////////////////////////////

bool ColorConvertParameters::operator==(const ColorConvertParameters& inOther) const
{
	return memcmp(this, &inOther, sizeof(ColorConvertParameters)) == 0;
}

bool ColorConvertParameters::operator!=(const ColorConvertParameters& inOther) const
{
	return !(*this == inOther);
}

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersPassthrough()
{
	return ColorConvertParameters(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

}

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersFromSRGBToLinear()
{
	return ColorConvertParameters(0.0773993808f, 0.04045f, 0.055f, 2.4f, 0.8794154614f, 0.0);

}

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersFromLinearToSRGB()
{
	return ColorConvertParameters(12.92f, 0.0031308f, 0.0f, 0.41666666666f, 1.055f, -0.055f);
}

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersFromPowerToLinear(float inPower)
{
	return ColorConvertParameters(0.0f, 0.0f, 0.0f, inPower, 1.0f, 0.0);
}

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersFromLinearToPower(float inPower)
{
	return ColorConvertParameters(0.0f, 0.0f, 0.0f, 1.0f / inPower, 1.0f, 0.0);
}

//////////////////////////////////////////////////////////////////////////////

bool gGetColorConvertParametersFromStringToLinear(wchar_t* inString, ColorConvertParameters& outColorConvertParameters)
{
	if (_wcsicmp(inString, L"sRGB") == 0)
	{
		outColorConvertParameters = gGetColorConvertParametersFromSRGBToLinear();
	}
	else 
	{
		wchar_t* string_end = inString;
		float power = (float)wcstod(inString, &string_end);

		if (power < 1.0f || *string_end != 0)
			return false;
		
		outColorConvertParameters = gGetColorConvertParametersFromPowerToLinear(power);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

bool gGetColorConvertParametersFromLinearToString(wchar_t* inString, ColorConvertParameters& outColorConvertParameters)
{
	if (_wcsicmp(inString, L"sRGB") == 0)
	{
		outColorConvertParameters = gGetColorConvertParametersFromLinearToSRGB();
	}
	else 
	{
		wchar_t* string_end = inString;
		float power = (float)wcstod(inString, &string_end);

		if (power < 1.0f || *string_end != 0)
			return false;
		
		outColorConvertParameters = gGetColorConvertParametersFromLinearToPower(power);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

float gConvertColor(const ColorConvertParameters& inParameters, float inColor)
{
	if (inColor < inParameters.mLinearThreshold)
		return inColor * inParameters.mLinearScale;

	return gPow(gMax(inColor + inParameters.mGammaPreBias, 0.0f), inParameters.mGammaPower) * inParameters.mGammaPostScale + inParameters.mGammaPostBias;
}

//////////////////////////////////////////////////////////////////////////////

Vec3 gConvertColor(const ColorConvertParameters& inParameters, const Vec3& inColor)
{
	return Vec3(gConvertColor(inParameters, inColor.GetX()),
				gConvertColor(inParameters, inColor.GetY()),
				gConvertColor(inParameters, inColor.GetZ()));
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility