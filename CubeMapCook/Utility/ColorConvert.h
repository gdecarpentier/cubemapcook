// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Math {
class Vec3;
} // namespace Math

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

struct ColorConvertParameters
{
public:
	ColorConvertParameters() { }
	ColorConvertParameters(float inLinearScale, float inLinearThreshold, float inGammaPreBias, float inGammaPower, float inGammaPostScale, float inGammaPostBias);

	bool operator==(const ColorConvertParameters& inOther) const;
	bool operator!=(const ColorConvertParameters& inOther) const;

	float mLinearScale;
	float mLinearThreshold;
	float mGammaPreBias;
	float mGammaPower;
	float mGammaPostScale;
	float mGammaPostBias;
};

//////////////////////////////////////////////////////////////////////////////

ColorConvertParameters gGetColorConvertParametersPassthrough();
ColorConvertParameters gGetColorConvertParametersFromSRGBToLinear();
ColorConvertParameters gGetColorConvertParametersFromLinearToSRGB();
ColorConvertParameters gGetColorConvertParametersFromPowerToLinear(float inPower);
ColorConvertParameters gGetColorConvertParametersFromLinearToPower(float inPower);
bool gGetColorConvertParametersFromStringToLinear(wchar_t* inString, ColorConvertParameters& outColorConvertParameters);
bool gGetColorConvertParametersFromLinearToString(wchar_t* inString, ColorConvertParameters& outColorConvertParameters);

//////////////////////////////////////////////////////////////////////////////

float gConvertColor(const ColorConvertParameters& inParameters, float inColor);
Math::Vec3 gConvertColor(const ColorConvertParameters& inParameters, const Math::Vec3& inColor);

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility