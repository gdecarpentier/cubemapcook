// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Log.h"
#include <stdarg.h>
#include <stdio.h>

namespace Utility {
namespace Internal {

//////////////////////////////////////////////////////////////////////////////

void gLog(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vwprintf(fmt, args);
	va_end(args);
	fflush(stdout);
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Internal
} // namespace Utility