// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

#define LOG_ERROR(fmt, ...) Utility::Internal::gLog(fmt, __VA_ARGS__)

#ifdef DEBUG
#define LOG_DEBUG(fmt, ...) Utility::Internal::gLog(fmt, __VA_ARGS__)
#else //DEBUG
#define LOG_DEBUG(fmt, ...) do {} while (0)
#endif

//////////////////////////////////////////////////////////////////////////////

namespace Internal
{

void gLog(const wchar_t* fmt, ...);

} // namespace Internal

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility