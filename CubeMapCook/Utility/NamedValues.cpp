// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "NamedValues.h"
#include <cstdio>
#include <cstring>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

uint32_t gLookupValueByName(const wchar_t *inName, const NamedValue *inNamedValues)
{
	while (inNamedValues->mName)
	{
		if (!_wcsicmp(inName, inNamedValues->mName))
			return inNamedValues->mValue;

		inNamedValues++;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////

const wchar_t* gLookupNameByValue(uint32_t inValue, const NamedValue *inNamedValues)
{
	while (inNamedValues->mName)
	{
		if (inValue == inNamedValues->mValue)
			return inNamedValues->mName;

		inNamedValues++;
	}

	return L"";
}

//////////////////////////////////////////////////////////////////////////////

void gPrintNamedValues(size_t inCursor, NamedValue *inNamedValuesBegin)
{
	while (inNamedValuesBegin->mName)
	{
		size_t name_length = wcslen(inNamedValuesBegin->mName);

		if (inCursor + name_length + 2 >= 80)
		{
			wprintf(L"\n      ");
			inCursor = 6;
		}

		wprintf(L"%ls ", inNamedValuesBegin->mName);
		inCursor += name_length + 2;
		inNamedValuesBegin++;
	}

	wprintf(L"\n");
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility