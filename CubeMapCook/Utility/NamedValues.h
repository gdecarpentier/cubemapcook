// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <stdint.h>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

struct NamedValue
{
	const wchar_t* mName;
	uint32_t mValue;
};

uint32_t gLookupValueByName(const wchar_t *inName, const NamedValue *inNamedValues);

const wchar_t* gLookupNameByValue(uint32_t inValue, const NamedValue *inNamedValues);

void gPrintNamedValues(size_t inCursor, NamedValue *inNamedValuesBegin);

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility