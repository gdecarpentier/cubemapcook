// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include <windows.h>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

typedef wchar_t Path[MAX_PATH];

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility