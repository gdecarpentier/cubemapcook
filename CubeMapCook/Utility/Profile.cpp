// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Profile.h"
#include "StopWatch.h"
#include <cstdio>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

namespace
{
	static const char* gProfileLevelTabs[] = 
	{
		"    ",	// PROFILE_LEVEL_LOW
		"  ",	// PROFILE_LEVEL_HIGH
		""		// PROFILE_LEVEL_SUMMARY
	};	

	thread_local static StopWatch gProfileStopWatches[PROFILE_LEVEL_COUNT];
}

//////////////////////////////////////////////////////////////////////////////

ProfileHelper::ProfileHelper(EProfileLevel inLevel, const char* inName) :
	mLevel(inLevel),
	mName(inName)
{
	assert(mLevel < PROFILE_LEVEL_COUNT);
	gProfileStopWatches[mLevel].Start();
}

//////////////////////////////////////////////////////////////////////////////

ProfileHelper::~ProfileHelper()
{
	gProfileStopWatches[mLevel].Stop();
	static_assert(sizeof(gProfileLevelTabs) / sizeof(gProfileLevelTabs[0]) == PROFILE_LEVEL_COUNT, "Unexpected number of profile levels");
	printf("%s%s: %.3f ms\n", gProfileLevelTabs[mLevel], mName, gProfileStopWatches[mLevel].TimeInMilliseconds());
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility