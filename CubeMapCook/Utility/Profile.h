// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once 

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

enum EProfileLevel
{
	PROFILE_LEVEL_LOW,
	PROFILE_LEVEL_HIGH,
	PROFILE_LEVEL_SUMMARY,
	PROFILE_LEVEL_COUNT
};

//////////////////////////////////////////////////////////////////////////////

#ifdef PROFILE 
#define ENABLE_PROFILE_LEVEL_LOW		1
#define ENABLE_PROFILE_LEVEL_HIGH		1
#define ENABLE_PROFILE_LEVEL_SUMMARY	1
#else
#define ENABLE_PROFILE_LEVEL_LOW		0
#define ENABLE_PROFILE_LEVEL_HIGH		0
#define ENABLE_PROFILE_LEVEL_SUMMARY	1
#endif

//////////////////////////////////////////////////////////////////////////////

#if ENABLE_PROFILE_LEVEL_LOW
#define PROFILE_LOW(name) ProfileHelper _profile_helper_low(PROFILE_LEVEL_LOW, name);
#else
#define PROFILE_LOW(name);
#endif

#if ENABLE_PROFILE_LEVEL_HIGH
#define PROFILE_HIGH(name) ProfileHelper _profile_helper_high(PROFILE_LEVEL_HIGH, name);
#else
#define PROFILE_HIGH(name);
#endif

#if ENABLE_PROFILE_LEVEL_SUMMARY
#define PROFILE_SUMMARY(name) ProfileHelper _profile_helper_summary(PROFILE_LEVEL_SUMMARY, name);
#else
#define PROFILE_SUMMARY(name);
#endif

//////////////////////////////////////////////////////////////////////////////

class ProfileHelper
{
public:
	ProfileHelper(EProfileLevel inLevel, const char* inName);
	~ProfileHelper();

private:
	EProfileLevel mLevel;
	const char* mName;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility