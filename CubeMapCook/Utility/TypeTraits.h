// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

struct NonCopyable
{
	NonCopyable() = default;
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
};

//////////////////////////////////////////////////////////////////////////////

template<int I>
struct IntType 
{ 
};

//////////////////////////////////////////////////////////////////////////////

template<typename TYPE> 
struct TypedInt
{
	explicit TypedInt(int inValue) :
		mValue(inValue)
	{
	}

	int& GetValue()
	{
		return mValue;
	}

	const int& GetValue() const
	{
		return mValue;
	}

private:
	int mValue;
};

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility