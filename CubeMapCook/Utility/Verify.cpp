// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"
#include "Log.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

void gVerify(bool inSucceeded, const wchar_t* fmt, ...)
{
	if (!inSucceeded)
	{
		va_list args;
		va_start(args, fmt);
		LOG_ERROR(fmt, args);
		va_end(args);
		assert(false);
		exit(1);
	}
}

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility