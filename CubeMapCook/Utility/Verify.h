// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#pragma once

#include <cassert>

namespace Utility {

//////////////////////////////////////////////////////////////////////////////

#if DEBUG
	#define ASSERT(inSucceeded) assert(inSucceeded)
#else
	#define ASSERT(inSucceeded) __assume(inSucceeded);
#endif

//////////////////////////////////////////////////////////////////////////////

void gVerify(bool inSucceeded, const wchar_t* fmt, ...);

//////////////////////////////////////////////////////////////////////////////

} // namespace Utility