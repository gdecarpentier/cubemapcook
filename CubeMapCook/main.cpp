// Copyright (c) 2016, Giliam de Carpentier. http://decarpentier.nl. See LICENSE.txt for the MIT license details.

#include "PCH.h"

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <memory>
#include <vector>

#include <dxgiformat.h>
#include "directxtex.h"

#include "ApplicationArguments.h"
#include "CubeIO.h"
#include "CubeConversion.h"
#include "Utility/Formats.h"
#include "Utility/ColorConvert.h"
#include "Utility/Log.h"
#include "Math/IVec.h"
#include "Math/SH1.h"
#include "Math/Algorithm.h"
#include "Math/Vec.h"
#include "Math/Mat.h"
#include "Cube.h"
#include "Utility/Profile.h"
#include "TaskManagement.h"
#include "GPU/ConstantBuffer.h"
#include "GPU/Device.h"
#include "GPU/Sampler.h"
#include "GPU/Shader.h"
#include "GPU/RWTextureView.h"
#include "GPU/Texture2D.h"
#include "GPU/TextureView.h"
#include "GPU/TextureCube.h"

#include "FreeImage.h"

using namespace DirectX;
using namespace GPU;
using namespace Math;
using namespace Utility;

//////////////////////////////////////////////////////////////////////////////

struct ShaderConstants
{
	union 
	{
		struct
		{
			IVec2 cDispatchThreadIDOffset;
			float cOutputScale;
		} AmbientBRDF;

		struct
		{
			float cRcpOutputResolution;
			float cInputLevel;
		} BilinearDownscale;
			
		struct
		{
			float cLinearScale;
			float cLinearThreshold;
			float cGammaPreBias;
			float cGammaPower;
			float cGammaPostScale;
			float cGammaPostBias;
		} ColorConvert;
			
		struct
		{
			Vec2 cRcpOutputResolution;
			float cInputLevel;
		} CubeToLongLat;

		struct
		{
			Vec4 cXYZCoordScale;
			Vec4 cXYZCoordBias;
			IVec2 cDispatchThreadIDOffset;
			float cAlpha;
			float cLevel;
			float cNextLevelAlpha;
			float cNextLevelWeight;
		} ImportanceGGX;

		struct
		{
			float cRcpHalfResolution;
		} MultiplySolidAngle;

	};
};

//////////////////////////////////////////////////////////////////////////////
// Generate a split-sum LUT texture based on the given parameters.

static bool sGenerateAmbientBRDF(
	int inWidth, 
	int inHeight, 
	const wchar_t* inAlphaExpression, 
	const wchar_t* inCosineExpression, 
	float inOutputScale, 
	const Utility::ColorConvertParameters& inColorConvertParametersOut, 
	DXGI_FORMAT inFormat,
	ScratchImage& outImage)
{	
	Context& context = gGetDevice().GetImmediateContext();

	//TODO give option to export to RG or RGB(A) texture (last is required for freeimage)

	Texture2D staging_uv_texture;
	staging_uv_texture.Initialize(inWidth, inHeight, 1,  DXGI_FORMAT_R32G32_FLOAT, CPUAccess::ReadWrite);

	Texture2D uv_texture;
	uv_texture.Initialize(inWidth, inHeight, 1, DXGI_FORMAT_R32G32_FLOAT, CPUAccess::None);

	Texture2D staging_output_texture;
	staging_output_texture.Initialize(inWidth, inHeight, 1, inFormat, CPUAccess::ReadWrite);

	Texture2D input_texture;
	input_texture.Initialize(inWidth, inHeight, 1, inFormat, CPUAccess::None);

	Texture2D output_texture;
	output_texture.Initialize(inWidth, inHeight, 1, inFormat, CPUAccess::None);

	outImage.Release();
	outImage.Initialize2D(inFormat, inWidth, inHeight, 1, 1);

	ConstantBuffer<ShaderConstants> constant_buffer;
	constant_buffer.Initialize();

	{
		PROFILE_HIGH("Upload");

		Vec2* buffer = new Vec2[inWidth * inHeight];

		for (int y = 0; y < inHeight; ++y)
		{
			for (int x = 0; x < inWidth; ++x)
			{
				float u = (0.5f + (float)x) / (float)inWidth;
				float v = (0.5f + (float)y) / (float)inHeight;

				float alpha;
				float cosine;
				if (!gEvaluateUVExpression(inAlphaExpression, u, v, alpha) ||
					!gEvaluateUVExpression(inCosineExpression, u, v, cosine))
				{
					return false;
				}

				buffer[y * inWidth + x] = Vec2(alpha, cosine);
			}
		}

		gUploadTexture2DArrayRaw(staging_uv_texture, buffer, sizeof(Vec2) * inWidth, 0, 0);
		delete [] buffer;

		context.Copy(staging_uv_texture, uv_texture);
	}

	{
		PROFILE_HIGH("AmbientBRDF");

		context.Bind(gGetPrecompiledShader(PrecompiledShader::AmbientBRDF));

		TextureView uv_texture_view;
		uv_texture_view.Initialize(uv_texture, 0, 0);
		context.Bind(&uv_texture_view, TextureSlot(0));

		RWTextureView output_rwtexture_view;
		output_rwtexture_view.Initialize(output_texture, 0);
		context.Bind(&output_rwtexture_view, UAVSlot(0));

		context.Bind(constant_buffer, ConstantBufferSlot(0));
		auto& constants = constant_buffer.GetConstants();
		constants.AmbientBRDF.cOutputScale = inOutputScale;

		int batch_size = 8;
		int batches_x = gMax(inWidth / batch_size, 1);
		int batches_y = gMax(inHeight / batch_size, 1);

		for (int batch_y = 0; batch_y < batches_x; ++batch_y)
		{	
			for (int batch_x = 0; batch_x < batches_y; ++batch_x)
			{
				constants.AmbientBRDF.cDispatchThreadIDOffset = IVec2(batch_x, batch_y) * batch_size;
				context.Upload(constant_buffer);

				context.Dispatch(IVec3(batch_size, batch_size, 1), IVec3(8, 8, 1));

				context.Flush();
			}
		}

		std::swap(input_texture, output_texture);
	}

	if (inColorConvertParametersOut != gGetColorConvertParametersPassthrough())
	{
		PROFILE_HIGH("ColorConvert");

		context.Bind(gGetPrecompiledShader(PrecompiledShader::ColorConvert));
		context.Bind(constant_buffer, ConstantBufferSlot(0));

		TextureView input_texture_view;
		input_texture_view.Initialize(input_texture, 0, 0);
		context.Bind(&input_texture_view, TextureSlot(0));

		RWTextureView output_rwtexture_view;
		output_rwtexture_view.Initialize(output_texture, 0, 0, 0);
		context.Bind(&output_rwtexture_view, UAVSlot(0));

		auto& constants = constant_buffer.GetConstants().ColorConvert;
		ASSERT(sizeof(ShaderConstants::ColorConvert) == sizeof(ColorConvertParameters));
		*reinterpret_cast<ColorConvertParameters*>(&constants) = inColorConvertParametersOut;

		context.Upload(constant_buffer);

		context.Dispatch(IVec3(inWidth, inHeight, 1), IVec3(8, 8, 1));

		context.Flush();

		std::swap(input_texture, output_texture);
	}

	{
		PROFILE_HIGH("Download");
		context.Copy(input_texture, staging_output_texture);
		gDownloadTexture2DArrayRaw(staging_output_texture, outImage.GetImages()->pixels, (int)outImage.GetImages()->rowPitch, 0, 0);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

// Filter and downsample a cube map using the given parameters
static bool sFilterCube(
	const wchar_t* inAlphaExpression, 
	const Utility::ColorConvertParameters& inColorConvertParametersIn, 
	const ScratchImage& inCube, 
	float inLinearScale, 
	const Utility::ColorConvertParameters& inColorConvertParametersOut, 
	bool inNormalizeIrradiance,
	ScratchImage& outCube)
{	
	{
		ASSERT(inCube.GetMetadata().format == DXGI_FORMAT_R32G32B32A32_FLOAT);
		ASSERT(inCube.GetMetadata().arraySize == 6);

		int resolution = (int)inCube.GetMetadata().width;
		ASSERT(resolution == (int)inCube.GetMetadata().height);
		int levels = 1 + (int)gCeil(gLog2((float)resolution));

		// Get the cube data in memory in a more convenient wrapper
		Cube input_cube;
		Cube output_cube;
		{
			PROFILE_HIGH("CubeInit");
			outCube.Release();
			outCube.InitializeCube(DXGI_FORMAT_R32G32B32A32_FLOAT, resolution, resolution, 1, levels);

			gGetCubeFromScratchImageMip(inCube, input_cube);
			gGetCubeFromScratchImageMip(outCube, output_cube);
		}

		{
			Context& context = gGetDevice().GetImmediateContext();

			// Define the samplers, constant buffers and ping/pong cube textures used for all GPU work.
			Sampler bilinear_sampler;
			bilinear_sampler.Initialize(TextureFilter::Bilinear, TextureAddress::Clamp);

			ConstantBuffer<ShaderConstants> constant_buffer;
			constant_buffer.Initialize();

			TextureCube staging_texture_cube;
			staging_texture_cube.Initialize(resolution, levels, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::ReadWrite);

			TextureCube output_texture_cube;
			output_texture_cube.Initialize(resolution, levels, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::None);

			TextureCube input_texture_cube;
			input_texture_cube.Initialize(resolution, levels, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::None);

			// Upload the input cube to the cube texture
			{
				PROFILE_HIGH("Upload");

				gUploadTextureCube(input_cube, staging_texture_cube, 0, 0);
				context.Copy(staging_texture_cube, output_texture_cube, 0, 0);

				std::swap(input_texture_cube, output_texture_cube);
			}

			// Convert the input in texture miplevel 0 from a perceptual space to (a scaled) linear space, if necessary.
			{
				if (inLinearScale != 1.0f || inColorConvertParametersIn != gGetColorConvertParametersPassthrough())
				{
					PROFILE_HIGH("ColorConvert");

					context.Bind(gGetPrecompiledShader(PrecompiledShader::ColorConvert));
					context.Bind(constant_buffer, ConstantBufferSlot(0));

					RWTextureView output_rwtexture_view;
					output_rwtexture_view.Initialize(output_texture_cube, 0);
					context.Bind(&output_rwtexture_view, UAVSlot(0));

					TextureView input_texture_view;
					input_texture_view.Initialize(input_texture_cube, 0, 0, 0, 5);
					context.Bind(&input_texture_view, TextureSlot(0));

					auto& constants = constant_buffer.GetConstants().ColorConvert;
					ASSERT(sizeof(ShaderConstants::ColorConvert) == sizeof(ColorConvertParameters));
					*reinterpret_cast<ColorConvertParameters*>(&constants) = inColorConvertParametersIn;

					constants.cGammaPostBias *= inLinearScale;
					constants.cGammaPostScale *= inLinearScale;
					constants.cLinearScale *= inLinearScale;

					context.Upload(constant_buffer);

					context.Dispatch(IVec3(resolution, resolution, 6), IVec3(8, 8, 1));

					context.Flush();

					std::swap(input_texture_cube, output_texture_cube);
				}
			}

			// Premultiply the color-converted input texture with the solid angle, allowing for better texel weighting later on.
			{
				PROFILE_HIGH("SolidAngleMultiply");

				context.Bind(gGetPrecompiledShader(PrecompiledShader::MultiplySolidAngle));
				context.Bind(constant_buffer, ConstantBufferSlot(0));

				RWTextureView output_rwtexture_view;
				output_rwtexture_view.Initialize(output_texture_cube, 0);
				context.Bind(&output_rwtexture_view, UAVSlot(0));

				TextureView input_texture_view;
				input_texture_view.Initialize(input_texture_cube, 0, 0, 0, 5);
				context.Bind(&input_texture_view, TextureSlot(0));

				auto& constants = constant_buffer.GetConstants().MultiplySolidAngle;
				constants.cRcpHalfResolution = 1.0f / (0.5f * resolution);
				context.Upload(constant_buffer);

				context.Dispatch(IVec3(resolution, resolution, 6), IVec3(8, 8, 1));

				context.Flush();

				std::swap(input_texture_cube, output_texture_cube);
			}

			SH1 sh1;
			if (inNormalizeIrradiance) 
			{
				PROFILE_HIGH("GetSH1");

				context.Copy(input_texture_cube, staging_texture_cube, 0, 0);
				gDownloadTextureCube(staging_texture_cube, output_cube, 0, 0);

				sh1 = gGetCubeSH1(output_cube, 0);
				gScaleSH1(sh1, 1.0f / 0.18f);
			}

			// Build a standard 2x2-downsampling mipmap texture chain of the solid-angle-weighted input 
			{
				PROFILE_HIGH("Downscale");

				context.Bind(gGetPrecompiledShader(PrecompiledShader::BilinearDownscale));
				context.Bind(bilinear_sampler, SamplerSlot(0));
				context.Bind(constant_buffer, ConstantBufferSlot(0));

				int output_resolution = resolution;
				for (int output_level = 1; output_level < levels; ++output_level)
				{ 
					output_resolution >>= 1;

					RWTextureView output_rwtexture_view;
					output_rwtexture_view.Initialize(input_texture_cube, output_level);
					context.Bind(&output_rwtexture_view, UAVSlot(0));

					TextureView input_texture_view;
					input_texture_view.Initialize(input_texture_cube, output_level - 1, output_level - 1, 0, 5);
					context.Bind(&input_texture_view, TextureSlot(0));

					auto& constants = constant_buffer.GetConstants().BilinearDownscale;
					constants.cInputLevel = (float)output_level - 1.0f;
					constants.cRcpOutputResolution = 1.0f / (float)output_resolution;
					context.Upload(constant_buffer);

					context.Dispatch(IVec3(output_resolution, output_resolution, 6), IVec3(8, 8, 1));

					context.Flush();
				}
			}
			
			// Do the actual filtering, going from the coarsest mip to the top one.
			{
				PROFILE_HIGH("Filter");

				context.Bind(gGetPrecompiledShader(PrecompiledShader::ImportanceGGX));
				context.Bind(bilinear_sampler, SamplerSlot(0));
				context.Bind(constant_buffer, ConstantBufferSlot(0));

				float next_level_alpha = 1.0E12f;

				LOG_DEBUG(L"  --------------------------------\n");
				LOG_DEBUG(L"  resolution  miplevel  alpha     \n");
				LOG_DEBUG(L"  --------------------------------\n");

				for (int output_level = levels - 1; output_level >= 0; --output_level)
				{ 
					int output_resolution = resolution >> output_level;

					RWTextureView output_rwtexture_view;
					output_rwtexture_view.Initialize(output_texture_cube, output_level);
					context.Bind(&output_rwtexture_view, UAVSlot(0));

					TextureView box_texture_view;
					box_texture_view.Initialize(input_texture_cube, 0, levels - 1);
					context.Bind(&box_texture_view, TextureSlot(0));

					TextureView next_level_texture_view;
					if (output_level + 1 < levels)
						next_level_texture_view.Initialize(output_texture_cube, output_level + 1, output_level + 1);
					else
						next_level_texture_view.Initialize(input_texture_cube, output_level, output_level);

					context.Bind(&next_level_texture_view, TextureSlot(1));

					float alpha;
					bool success = gEvaluateResolutionLevelExpression(inAlphaExpression, output_resolution, output_level, alpha);
					ASSERT(success);

					LOG_DEBUG(L"  %10d %9d  %.8f\n", output_resolution, output_level, alpha);
					if (alpha > 1.0f/4096.0f)
					{
						alpha = gMax(alpha, 1.0f/4096.0f);
						float next_level_weight = (alpha * (1.0f + next_level_alpha)) / (next_level_alpha * (1.0f + alpha));	
						next_level_weight = gMin(next_level_weight, 0.5f);

						auto& constants = constant_buffer.GetConstants().ImportanceGGX;
						constants.cAlpha = alpha;
						constants.cLevel = (float)output_level;
						constants.cNextLevelAlpha = next_level_alpha > 1.0f ? 1.0f : next_level_alpha;
						constants.cNextLevelWeight = next_level_alpha > 1.0f ? 0.0f : next_level_weight;

						int batch_size = 64;
						int batches_per_dimension = gMax(output_resolution / batch_size, 1);

						for (int face = 0; face < 6; ++face)
						{
							for (int batch_y = 0; batch_y < batches_per_dimension; ++batch_y)
							{	
								for (int batch_x = 0; batch_x < batches_per_dimension; ++batch_x)
								{
									gGetUVToXYZScaleBias(output_resolution, face, constants.cXYZCoordScale, constants.cXYZCoordBias);
									constants.cDispatchThreadIDOffset = IVec2(batch_x, batch_y) * batch_size;

									context.Upload(constant_buffer);
									context.Dispatch(IVec3(gMin(output_resolution - constants.cDispatchThreadIDOffset.GetX(), batch_size),
														   gMin(output_resolution - constants.cDispatchThreadIDOffset.GetY(), batch_size),
														   1), 
													 IVec3(8, 8, 1));

									context.Flush();
								}
							}
						}

						next_level_alpha = alpha;
					}
					else
					{
						context.Copy(input_texture_cube, output_texture_cube, output_level, output_level);
					}
				}

				LOG_DEBUG(L"  --------------------------------\n");

				std::swap(input_texture_cube, output_texture_cube);
			}

			// Weigh the input by the inverse of its SH1, effectively normalizing it to its irradiance
			if (inNormalizeIrradiance) 
			{
				PROFILE_HIGH("DivideBySH1");

				int last_level = staging_texture_cube.GetLevels() - 1;
				context.Copy(input_texture_cube, staging_texture_cube);
				gDownloadTextureCube(staging_texture_cube, output_cube, 0, last_level);

				// proces output_cube
				for (int level = 0; level <= last_level; ++level)
				{
					gDivideCubeBySH1(output_cube, level, sh1);
				}

				gUploadTextureCube(output_cube, staging_texture_cube, 0, staging_texture_cube.GetLevels() - 1);
				context.Copy(staging_texture_cube, output_texture_cube);

				std::swap(input_texture_cube, output_texture_cube);
			}

			// Convert to an perceptual output color space, if necessary.
			{
				if (inColorConvertParametersOut != gGetColorConvertParametersPassthrough())
				{
					PROFILE_HIGH("ColorConvert");

					context.Bind(gGetPrecompiledShader(PrecompiledShader::ColorConvert));
					context.Bind(constant_buffer, ConstantBufferSlot(0));

					for (int level = 0; level < levels; ++level)
					{
						TextureView input_texture_view;
						input_texture_view.Initialize(input_texture_cube, level, level, 0, 5);
						context.Bind(&input_texture_view, TextureSlot(0));

						RWTextureView output_rwtexture_view;
						output_rwtexture_view.Initialize(output_texture_cube, level);
						context.Bind(&output_rwtexture_view, UAVSlot(0));

						auto& constants = constant_buffer.GetConstants().ColorConvert;
						ASSERT(sizeof(ShaderConstants::ColorConvert) == sizeof(ColorConvertParameters));
						*reinterpret_cast<ColorConvertParameters*>(&constants) = inColorConvertParametersOut;

						context.Upload(constant_buffer);

						context.Dispatch(IVec3(resolution, resolution, 6), IVec3(8, 8, 1));

						context.Flush();
					}

					std::swap(input_texture_cube, output_texture_cube);
				}
			}

			// Get the finished cubemap back in main memory.
			{
				PROFILE_HIGH("Download");

				context.Copy(input_texture_cube, staging_texture_cube);
				gDownloadTextureCube(staging_texture_cube, output_cube, 0, staging_texture_cube.GetLevels() - 1);
			}
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

static bool sGenerateAmbientBRDF(const ApplicationArguments& inArguments)
{
	ScratchImage ambientbrdf_image;
	bool success;

	wchar_t ext[_MAX_EXT];
	wchar_t fname[_MAX_FNAME];
	_wsplitpath_s(inArguments.mOutputFilename, nullptr, 0, nullptr, 0, fname, _MAX_FNAME, ext, _MAX_EXT);

	int width = inArguments.mSize;
	int height = inArguments.mSize;
	bool output_dds =  _wcsicmp(ext, L".dds") == 0;
	DXGI_FORMAT format = output_dds ? DXGI_FORMAT_R16G16_FLOAT : DXGI_FORMAT_R32G32B32A32_FLOAT;

	// Generate a ambient brdf lookup image
	{
		success = sGenerateAmbientBRDF(
			width, 
			height, 
			inArguments.mAlphaExpression, 
			inArguments.mCosineExpression,
			inArguments.mLinearScale, 
			inArguments.mOutputColorConvertParameters, 
			format,
			ambientbrdf_image);

		if (!success)
		{
			LOG_ERROR(L"Failed to generate data for %ls.\n", inArguments.mOutputFilename);
			return false;
		}
	}

	// Write the converted cube to disk
	{
		PROFILE_HIGH("Write");

		LOG_DEBUG(L"Writing %ls...\n", inArguments.mOutputFilename);

		if (output_dds)
		{
			HRESULT hr = SaveToDDSFile(
				ambientbrdf_image.GetImages(),
				ambientbrdf_image.GetImageCount(),
				ambientbrdf_image.GetMetadata(),
				DDS_FLAGS_NONE,
				inArguments.mOutputFilename);

			success = SUCCEEDED(hr);
		}
		else
		{
			FIBITMAP* bitmap = FreeImage_AllocateT(FIT_RGBAF, width, height, 128);
			gVerify(FreeImage_GetPitch(bitmap) == ambientbrdf_image.GetImages()->rowPitch, L"Failed to initialize image");
			//TODO flip vertically
			memcpy(FreeImage_GetBits(bitmap), ambientbrdf_image.GetImages()->pixels, (int)ambientbrdf_image.GetImages()->rowPitch * height);
			FreeImage_SaveU(FIF_TIFF, bitmap, inArguments.mOutputFilename);
			FreeImage_Unload(bitmap);
		}

		if (!success)
		{
			LOG_ERROR(L"Failed to write %ls.\n", inArguments.mOutputFilename);
			return false;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

// Filter and downsample a cube map using the given parameters
static bool sGetLongLat(const Utility::ColorConvertParameters& inColorConvertParametersIn, const ScratchImage& inCube, int inWidth, int inHeight, float inLinearScale, ScratchImage& outImage)
{	
	{
		ASSERT(inCube.GetMetadata().format == DXGI_FORMAT_R32G32B32A32_FLOAT);
		ASSERT(inCube.GetMetadata().arraySize == 6);

		outImage.Release();
		outImage.Initialize2D(DXGI_FORMAT_R32G32B32A32_FLOAT, inWidth, inHeight, 1, 1);

		int resolution = (int)inCube.GetMetadata().width;
		ASSERT(resolution == (int)inCube.GetMetadata().height);

		// Get the cube data in memory in a more convenient wrapper
		Cube input_cube;
		gGetCubeFromScratchImageMip(inCube, input_cube);

		Context& context = gGetDevice().GetImmediateContext();

		// Define the samplers, constant buffers and ping/pong cube textures used for all GPU work.
		Sampler bilinear_sampler;
		bilinear_sampler.Initialize(TextureFilter::Bilinear, TextureAddress::Clamp);

		ConstantBuffer<ShaderConstants> constant_buffer;
		constant_buffer.Initialize();

		TextureCube staging_texture_cube;
		staging_texture_cube.Initialize(resolution, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::Write);

		TextureCube input_texture_cube;
		input_texture_cube.Initialize(resolution, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::None);

		TextureCube output_texture_cube;
		output_texture_cube.Initialize(resolution, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::None);

		Texture2D output_texture_image;
		output_texture_image.Initialize(inWidth, inHeight, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::None);

		Texture2D staging_texture_image;
		staging_texture_image.Initialize(inWidth, inHeight, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, CPUAccess::Read);

		// Upload the input cube to the cube texture
		{
			PROFILE_HIGH("Upload");

			gUploadTextureCube(input_cube, staging_texture_cube, 0, 0);
			context.Copy(staging_texture_cube, output_texture_cube, 0, 0);

			std::swap(input_texture_cube, output_texture_cube);
		}

		// Convert the input in texture miplevel 0 from a perceptual space to (a scaled) linear space, if necessary.
		if (inLinearScale != 1.0f || inColorConvertParametersIn != gGetColorConvertParametersPassthrough())
		{
			PROFILE_HIGH("ColorConvert");

			context.Bind(gGetPrecompiledShader(PrecompiledShader::ColorConvert));
			context.Bind(constant_buffer, ConstantBufferSlot(0));

			TextureView input_texture_view;
			input_texture_view.Initialize(input_texture_cube, 0, 0, 0, 5);
			context.Bind(&input_texture_view, TextureSlot(0));

			RWTextureView output_rwtexture_view;
			output_rwtexture_view.Initialize(output_texture_cube, 0);
			context.Bind(&output_rwtexture_view, UAVSlot(0));

			auto& constants = constant_buffer.GetConstants().ColorConvert;
			ASSERT(sizeof(ShaderConstants::ColorConvert) == sizeof(ColorConvertParameters));
			*reinterpret_cast<ColorConvertParameters*>(&constants) = inColorConvertParametersIn;

			constants.cGammaPostBias *= inLinearScale;
			constants.cGammaPostScale *= inLinearScale;
			constants.cLinearScale *= inLinearScale;

			context.Upload(constant_buffer);

			context.Dispatch(IVec3(resolution, resolution, 6), IVec3(8, 8, 1));

			context.Flush();

			std::swap(input_texture_cube, output_texture_cube);
		}

		// Do the actual mapping.
		{
			PROFILE_HIGH("Filter");

			context.Bind(gGetPrecompiledShader(PrecompiledShader::CubeToLongLat));
			context.Bind(bilinear_sampler, SamplerSlot(0));
			context.Bind(constant_buffer, ConstantBufferSlot(0));

			TextureView cube_view;
			cube_view.Initialize(input_texture_cube, 0, 0);
			context.Bind(&cube_view, TextureSlot(0));

			RWTextureView output_rwtexture_view;
			output_rwtexture_view.Initialize(output_texture_image, 0);
			context.Bind(&output_rwtexture_view, UAVSlot(0));

			auto& constants = constant_buffer.GetConstants().CubeToLongLat;
			constants.cRcpOutputResolution = Vec2(1.0f / (float)inWidth, 1.0f / (float)inHeight);
			constants.cInputLevel = 0;
			context.Upload(constant_buffer);

			context.Dispatch(IVec3(inWidth, inHeight, 1), IVec3(8, 8, 1));

			context.Flush();
		}

		// Get the finished texture back in main memory.
		{
			PROFILE_HIGH("Download");

			context.Copy(output_texture_image, staging_texture_image);
			gDownloadTexture2DArrayRaw(staging_texture_image, outImage.GetImages()->pixels, (int)outImage.GetImages()->rowPitch, 0, 0);
		}
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////////

static bool sGenerateFilteredCube(const ApplicationArguments& inArguments)
{
	// Generate a filter cube map chain.

	bool success;
	DXGI_FORMAT output_format = inArguments.mOutputFormat;

	// Read the cube (faces) from disk and assemble a single ScratchImage containing 6 faces with float4 texels
	ScratchImage float_cube;
	{
		PROFILE_HIGH("Read and convert");

		ScratchImage input_cube;
		success = gReadAndAssembleCube(inArguments.mInputFilenames, input_cube, output_format);
		if (!success)
			return false;

		// Convert the cubemap to float4 data
		success = gConvertCube(input_cube, float_cube, DXGI_FORMAT_R32G32B32A32_FLOAT);
		if (!success)
			return false;
	}

	// Re-orient the faces, if necessary
	{
		PROFILE_HIGH("Orient");

		gReorientCubeFaces(float_cube, inArguments.mInputOrientations);
	}

	// Process the cubemap
	ScratchImage processed_cube;
	success = sFilterCube(
		inArguments.mAlphaExpression,
		inArguments.mInputColorConvertParameters,
		float_cube, 
		inArguments.mLinearScale,
		inArguments.mOutputColorConvertParameters,
		(inArguments.mOptions & (1 << ApplicationArguments::OPT_NORMALIZE_IRRADIANCE)) != 0,
		processed_cube);

	if (!success)
		return false;

	// Re-orient the faces, if necessary
	{
		PROFILE_HIGH("Orient");

		gReorientCubeFaces(processed_cube, inArguments.mOutputOrientations);
	}

	// Convert the processed cube to the desired output format, if necessary
	ScratchImage output_cube;
	{
		PROFILE_HIGH("Convert");

		//Use the default output format if no preference was specified
		if (output_format == DXGI_FORMAT_UNKNOWN)
			output_format = DXGI_FORMAT_R16G16B16A16_FLOAT;

		// Convert the processed cube to the specified output_format
		success = gConvertCube(processed_cube, output_cube, output_format);
		if (!success)
			return false;
	}

	// Write the converted cube to disk
	{
		PROFILE_HIGH("Write");

		success = gWriteCube(inArguments.mOutputFilename, output_cube, (inArguments.mOptions & (1 << ApplicationArguments::OPT_DX10)) != 0);
		if (!success)
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

static bool sGenerateLongLat(const ApplicationArguments& inArguments)
{
	// Generate a filter cube map chain.

	bool success;

	wchar_t ext[_MAX_EXT];
	wchar_t fname[_MAX_FNAME];
	_wsplitpath_s(inArguments.mOutputFilename, nullptr, 0, nullptr, 0, fname, _MAX_FNAME, ext, _MAX_EXT);

	int width = 2 * inArguments.mSize;
	int height = inArguments.mSize;
	bool output_dds =  _wcsicmp(ext, L".dds") == 0;
	DXGI_FORMAT output_format = output_dds ? DXGI_FORMAT_R16G16_FLOAT : DXGI_FORMAT_R32G32B32A32_FLOAT;
	DXGI_FORMAT dummy_format;

	// Read the cube (faces) from disk and assemble a single ScratchImage containing 6 faces with float4 texels
	ScratchImage float_cube;
	{
		PROFILE_HIGH("Read and convert");

		ScratchImage input_cube;
		success = gReadAndAssembleCube(inArguments.mInputFilenames, input_cube, dummy_format);
		if (!success)
			return false;

		// Convert the cubemap to float4 data
		success = gConvertCube(input_cube, float_cube, DXGI_FORMAT_R32G32B32A32_FLOAT);
		if (!success)
			return false;
	}

	// Re-orient the faces, if necessary
	{
		PROFILE_HIGH("Orient");

		gReorientCubeFaces(float_cube, inArguments.mInputOrientations);
	}

	// Process the cubemap
	ScratchImage processed_longlat;
	success = sGetLongLat(
		inArguments.mInputColorConvertParameters,
		float_cube,
		width,
		height,
		inArguments.mLinearScale,
		processed_longlat);

	if (!success)
		return false;

	// Convert the processed cube to the desired output format, if necessary
	ScratchImage output_longlat;
	{
		PROFILE_HIGH("Convert");

		//Use the default output format if no preference was specified
		if (output_format == DXGI_FORMAT_UNKNOWN)
			output_format = DXGI_FORMAT_R16G16B16A16_FLOAT;

		if (processed_longlat.GetMetadata().format == output_format)
		{
			output_longlat = std::move(processed_longlat);
		}
		else if (!IsCompressed(output_format))
		{
			HRESULT hr = Convert(processed_longlat.GetImages(), processed_longlat.GetImageCount(), processed_longlat.GetMetadata(), output_format, TEX_FILTER_DEFAULT | TEX_FILTER_FORCE_NON_WIC, 0.5f, output_longlat);
			if (FAILED(hr))
			{
				LOG_ERROR(L" FAILED [convert to output format] (%x)\n", hr);
				return false;
			}
		}
	}

	// Write the converted cube to disk
	{
		PROFILE_HIGH("Write");

		LOG_DEBUG(L"Writing %ls...\n", inArguments.mOutputFilename);

		if (output_dds)
		{
			HRESULT hr = SaveToDDSFile(
				output_longlat.GetImages(),
				output_longlat.GetImageCount(),
				output_longlat.GetMetadata(),
				DDS_FLAGS_NONE,
				inArguments.mOutputFilename);

			success = SUCCEEDED(hr);
		}
		else
		{
			FIBITMAP* bitmap = FreeImage_AllocateT(FIT_RGBAF, width, height, 128);
			gVerify(FreeImage_GetPitch(bitmap) == output_longlat.GetImages()->rowPitch, L"Failed to initialize image");
			//TODO flip vertically
			memcpy(FreeImage_GetBits(bitmap), output_longlat.GetImages()->pixels, (int)output_longlat.GetImages()->rowPitch * height);
			FreeImage_SaveU(FIF_TIFF, bitmap, inArguments.mOutputFilename);
			FreeImage_Unload(bitmap);
		}

		if (!success)
		{
			LOG_ERROR(L"Failed to write %ls.\n", inArguments.mOutputFilename);
			return false;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////

static bool sRun(const ApplicationArguments& inArguments)
{
	if ((inArguments.mOptions & (1 << ApplicationArguments::OPT_AMBIENT_BRDF)) != 0)
	{
		return sGenerateAmbientBRDF(inArguments);

	}
	else if ((inArguments.mOptions & (1 << ApplicationArguments::OPT_TO_LONG_LAT)) != 0)
	{
		return sGenerateLongLat(inArguments);
	}
	else
	{
		return sGenerateFilteredCube(inArguments);
	}
}

//////////////////////////////////////////////////////////////////////////////
//#pragma warning( suppress : 6262 )
int __cdecl wmain(int inArgC, wchar_t* inArgV[])
{
	PROFILE_SUMMARY("Total");

	ApplicationArguments application_arguments;
	bool success = gGetApplicationArguments(inArgC, inArgV, application_arguments);
	if (!success)
	{
		gPrintApplicationArgumentsHelp();
		return 1;
	}

	FreeImage_Initialise(TRUE);
	gInitISPCTaskManagement(THREAD_PRIORITY_BELOW_NORMAL);
	gGetDevice().Initialize();

	gInitializePrecompiledShaders();

	success = sRun(application_arguments);
	if (!success)
		LOG_ERROR(L"\nFAILED\n");

	gFinalizePrecompiledShaders();
	gGetDevice().Finalize();
	FreeImage_DeInitialise();

	return success ? 0 : 1;
}
