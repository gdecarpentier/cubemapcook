# CubeMapCook #

CubeMapCook is a tool that generates and exports high-quality filtered cube mipmap chains.

It's capable of importing from and exporting to many different LDR/HDR file and color formats, including BC1-BC7 compressed DDS files.

Controlled using command line arguments, it will:

* Load separate faces from 6 image files, or 1 cube directly from a DDS file.
* Apply the specified degamma conversion (if any) and re-orient the faces if necessary.
* Build a cubemap mip chain, filtering each mipmap level with a roughness/alpha calculated from the mipmap level/resolution using a formula specified as a command line argument.
* Apply the specified gamma conversion (if any) mip level, and re-orient the faces if necessary.
* Export the mipmap chain to a DDS file with the specified format (like BC6H or BC7).

Currently, the only available filter is the cosine-weighted GGX kernel, but more filter types may be implemented in the future.

Filtering is performed on the GPU instead of the CPU to speed up this process as much as possible.
BC6 compression is done on all cores using Intel's ISPC compressor.

## Requirements ##

This software can only run on Windows, and requires a GPU supporting the full DirectX 11.0 feature set.

The source code may be built using Microsoft Visual Studio 2015.

## License ##

This work is freely available for both commercial and non-commercial use provided you agree with the (very permissive and simple) conditions of its [MIT](https://en.wikipedia.org/wiki/MIT_License) license.

## Author ##
Giliam de Carpentier

[www.decarpentier.nl](http://www.decarpentier.nl)